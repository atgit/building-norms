﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuildingNorms
{
    public partial class AddNorm : Form
    {
        Item newNorm;
        public AddNorm()
        {
            InitializeComponent();
        }

        public Item NewNorm { get => newNorm; set => newNorm = value; }

        private void AddNorm_Load(object sender, EventArgs e)
        {
            Txt_Cat.Text = NewNorm.CatNumber;
            Txt_Mach.Text = NewNorm.Mach;
            Txt_Group.Text = NewNorm.Group.ToString();
        }
    }

    
}
