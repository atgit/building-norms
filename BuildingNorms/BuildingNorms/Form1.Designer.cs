﻿namespace BuildingNorms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label4 = new System.Windows.Forms.Label();
            this.GB_Groups = new System.Windows.Forms.GroupBox();
            this.Btn_Update = new System.Windows.Forms.Button();
            this.DGV_Groups = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.Txt_Level1 = new System.Windows.Forms.TextBox();
            this.Btn_Level1 = new System.Windows.Forms.Button();
            this.GB_Groups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Groups)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.LightBlue;
            this.label4.Location = new System.Drawing.Point(654, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(228, 42);
            this.label4.TabIndex = 251;
            this.label4.Text = "אשף הניהול";
            // 
            // GB_Groups
            // 
            this.GB_Groups.Controls.Add(this.Btn_Update);
            this.GB_Groups.Controls.Add(this.DGV_Groups);
            this.GB_Groups.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.GB_Groups.Location = new System.Drawing.Point(12, 147);
            this.GB_Groups.Name = "GB_Groups";
            this.GB_Groups.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.GB_Groups.Size = new System.Drawing.Size(278, 458);
            this.GB_Groups.TabIndex = 252;
            this.GB_Groups.TabStop = false;
            this.GB_Groups.Text = "קבוצות מכונות";
            // 
            // Btn_Update
            // 
            this.Btn_Update.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Update.Location = new System.Drawing.Point(75, 419);
            this.Btn_Update.Name = "Btn_Update";
            this.Btn_Update.Size = new System.Drawing.Size(127, 28);
            this.Btn_Update.TabIndex = 253;
            this.Btn_Update.Text = "עדכן קבוצות";
            this.Btn_Update.UseVisualStyleBackColor = true;
            this.Btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // DGV_Groups
            // 
            this.DGV_Groups.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Groups.Location = new System.Drawing.Point(6, 25);
            this.DGV_Groups.Name = "DGV_Groups";
            this.DGV_Groups.ReadOnly = true;
            this.DGV_Groups.Size = new System.Drawing.Size(260, 388);
            this.DGV_Groups.TabIndex = 0;
            this.DGV_Groups.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_Groups_CellValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(363, 149);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(63, 19);
            this.label2.TabIndex = 257;
            this.label2.Text = "שלב א\'";
            // 
            // Txt_Level1
            // 
            this.Txt_Level1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Txt_Level1.Location = new System.Drawing.Point(345, 187);
            this.Txt_Level1.Name = "Txt_Level1";
            this.Txt_Level1.Size = new System.Drawing.Size(100, 27);
            this.Txt_Level1.TabIndex = 256;
            this.Txt_Level1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Btn_Level1
            // 
            this.Btn_Level1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Btn_Level1.Location = new System.Drawing.Point(346, 220);
            this.Btn_Level1.Name = "Btn_Level1";
            this.Btn_Level1.Size = new System.Drawing.Size(99, 56);
            this.Btn_Level1.TabIndex = 258;
            this.Btn_Level1.Text = "עדכן יחסים";
            this.Btn_Level1.UseVisualStyleBackColor = true;
            this.Btn_Level1.Click += new System.EventHandler(this.Btn_Level1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BuildingNorms.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(1469, 634);
            this.Controls.Add(this.Btn_Level1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Txt_Level1);
            this.Controls.Add(this.GB_Groups);
            this.Controls.Add(this.label4);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Building Norms";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GB_Groups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Groups)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox GB_Groups;
        private System.Windows.Forms.DataGridView DGV_Groups;
        private System.Windows.Forms.Button Btn_Update;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox Txt_Level1;
        private System.Windows.Forms.Button Btn_Level1;
    }
}

