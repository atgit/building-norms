﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuildingNorms
{
    public partial class Form1 : Form
    {
        DBService dbs = new DBService();
        DataTable DT = new DataTable();
        OleDbDataAdapter da = new OleDbDataAdapter();
        string StrSql = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillGroupData();
            Txt_Level1.Text = GetRelations().Item1.ToString();
        }

        public void FillGroupData()
        {
            DGV_Groups.CellValueChanged -= DGV_Groups_CellValueChanged;
            DGV_Groups.Rows.Clear();

            String query = @"select distinct MOMACH,MOFRE1
                            FROM BPCSFALI.LMHO
                            WHERE MOWRKC between 750000 and 760000 and MOFRE1 <>''";
            da = new OleDbDataAdapter(query, dbs.conn);
            OleDbCommandBuilder cb = new OleDbCommandBuilder(da);
            DT = new DataTable();
            da.Fill(DT);
            DGV_Groups.DataSource = DT;
            dbs.conn.Close();

            foreach (DataGridViewColumn column in DGV_Groups.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Automatic;
                column.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            DGV_Groups.Columns["MOMACH"].HeaderText = "מכונה";
            DGV_Groups.Columns["MOFRE1"].HeaderText = "קבוצה";

            DGV_Groups.ReadOnly = false;
            foreach (DataGridViewColumn c in DGV_Groups.Columns)
            {
                c.ReadOnly = true;
                if (c.DataPropertyName == "MOFRE1")
                {
                    c.ReadOnly = false;  // editable
                    c.DefaultCellStyle.BackColor = Color.LightYellow;
                }
            }

            DGV_Groups.Refresh();
            DGV_Groups.CellValueChanged += DGV_Groups_CellValueChanged;
        }

        public static double GetFactor()
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            string StrSql = @"SELECT Value 
                              FROM Building_Norm_Params
                              WHERE Param_Name = ? ";
            SqlParameter param = new SqlParameter("param", "Factor");
            DT = dbs.GetDataSetByQuery(StrSql, CommandType.Text, param).Tables[0];
            return double.Parse(DT.Rows[0]["Value"].ToString());
        }

        public static void UpdateFactor(double factor)
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            string StrSql = @"UPDATE Building_Norm_Params
                       SET Value = ?
                       WHERE Param_Name = ?";
            SqlParameter value = new SqlParameter("val", factor);
            SqlParameter param = new SqlParameter("param", "Factor");
            dbs.ExecuteQuery(StrSql, CommandType.Text,value,param);
        }

        public static Tuple<double,double> GetRelations()
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            string StrSql = @"SELECT Value
                       FROM Building_Norm_Params
                       WHERE Param_Name in(?,?)";
            SqlParameter carcas = new SqlParameter("carcas", "Carcas");
            SqlParameter green = new SqlParameter("green", "Green");
            DT = dbs.GetDataSetByQuery(StrSql, CommandType.Text, carcas,green).Tables[0];
            return Tuple.Create(double.Parse(DT.Rows[0]["Value"].ToString()),double.Parse(DT.Rows[1]["Value"].ToString()));
        }

        public void UpdateRelations()
        {
            DbServiceSQL dbs = new DbServiceSQL();
            StrSql = @"UPDATE Building_Norm_Params
                       SET Value = ?
                       WHERE Param_Name = ?";
            // עדכון שלב א
            SqlParameter value = new SqlParameter("val", Txt_Level1.Text);
            SqlParameter param = new SqlParameter("param", "Carcas");
            dbs.ExecuteQuery(StrSql, CommandType.Text, value, param);
            // עדכון שלב ב
            value = new SqlParameter("val", 1 - decimal.Parse(Txt_Level1.Text));
            param = new SqlParameter("param", "Green");
            dbs = new DbServiceSQL();
            dbs.ExecuteQuery(StrSql, CommandType.Text, value, param);
        }

        /// <summary>
        /// מתודת עדכון קבוצות המכונות 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_Update_Click(object sender, EventArgs e)
        {
            DialogResult D = MessageBox.Show("?אתה בטוח שאתה רוצה לעדכן את הקבוצות");
            if (D == DialogResult.OK)
            {
                DataTable ds1 = ((DataTable)DGV_Groups.DataSource).GetChanges();
                //da.Update(ds1);
                foreach (DataRow row in ds1.Rows)
                {
                    StrSql = $@"UPDATE BPCSFALI.LMHO 
                                SET MOFRE1 ='{row["MOFRE1"]}'
                                WHERE MOMACH = '{row["MOMACH"]}' AND MOWRKC BETWEEN 750000 AND 760000 ";
                    dbs.executeUpdateQuery(StrSql);
                }
                
            }
        }

        private void DGV_Groups_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //DGV_Groups.Rows[e.RowIndex].Cells[e.ColumnIndex].InitializeEditingControl.dir
            DGV_Groups.NotifyCurrentCellDirty(true);
            DGV_Groups.NotifyCurrentCellDirty(false);
            bool b = DGV_Groups.IsCurrentCellDirty;
        }

        private void Btn_Level1_Click(object sender, EventArgs e)
        {
            UpdateRelations();
        }
    }
}
