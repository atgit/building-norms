﻿namespace BuildingNorms
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Txt_Cat = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabTeken = new System.Windows.Forms.TabPage();
            this.Btn_UpdateFactor = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.Txt_Factor = new System.Windows.Forms.TextBox();
            this.Txt_JumboMain = new System.Windows.Forms.TextBox();
            this.Txt_TiresInShfitMain = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.Txt_Mach = new System.Windows.Forms.TextBox();
            this.Btn_UpdateNorm = new System.Windows.Forms.Button();
            this.Lbl_NormCount = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Histogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabEdit = new System.Windows.Forms.TabPage();
            this.Txt_JumboNorm = new System.Windows.Forms.TextBox();
            this.Txt_TiresInShift = new System.Windows.Forms.TextBox();
            this.Lbl_CountGaps = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.Btn_Update = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.Lbl_Gaps = new System.Windows.Forms.Label();
            this.Txt_Percentage = new System.Windows.Forms.TextBox();
            this.Btn_Gaps = new System.Windows.Forms.Button();
            this.DGV_Gaps = new System.Windows.Forms.DataGridView();
            this.tabNew = new System.Windows.Forms.TabPage();
            this.Lbl_Prog = new System.Windows.Forms.Label();
            this.PB_NewItems = new System.Windows.Forms.ProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Grp_Jumbo = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Txt_Minutes = new System.Windows.Forms.TextBox();
            this.Txt_Calc = new System.Windows.Forms.TextBox();
            this.Lbl_Tires = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.Txt_Green = new System.Windows.Forms.TextBox();
            this.Txt_CatNum = new System.Windows.Forms.TextBox();
            this.DGV_Weightings = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.Btn_NewNorm = new System.Windows.Forms.Button();
            this.DGV_Similar = new System.Windows.Forms.DataGridView();
            this.DGV_New = new System.Windows.Forms.DataGridView();
            this.btnNew = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabTeken.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Histogram)).BeginInit();
            this.tabEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Gaps)).BeginInit();
            this.tabNew.SuspendLayout();
            this.Grp_Jumbo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Weightings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Similar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_New)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(8, 49);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(1882, 433);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView1_CellBeginEdit);
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentDoubleClick);
            this.dataGridView1.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dataGridView1.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridView1_CellValidating);
            // 
            // Txt_Cat
            // 
            this.Txt_Cat.Location = new System.Drawing.Point(59, 23);
            this.Txt_Cat.Name = "Txt_Cat";
            this.Txt_Cat.Size = new System.Drawing.Size(188, 22);
            this.Txt_Cat.TabIndex = 1;
            this.Txt_Cat.Visible = false;
            this.Txt_Cat.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabTeken);
            this.tabControl1.Controls.Add(this.tabEdit);
            this.tabControl1.Controls.Add(this.tabNew);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabControl1.Location = new System.Drawing.Point(0, 142);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1904, 900);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tabControl1.TabIndex = 2;
            // 
            // tabTeken
            // 
            this.tabTeken.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabTeken.Controls.Add(this.Btn_UpdateFactor);
            this.tabTeken.Controls.Add(this.label13);
            this.tabTeken.Controls.Add(this.Txt_Factor);
            this.tabTeken.Controls.Add(this.Txt_JumboMain);
            this.tabTeken.Controls.Add(this.Txt_TiresInShfitMain);
            this.tabTeken.Controls.Add(this.label12);
            this.tabTeken.Controls.Add(this.Txt_Mach);
            this.tabTeken.Controls.Add(this.Btn_UpdateNorm);
            this.tabTeken.Controls.Add(this.Lbl_NormCount);
            this.tabTeken.Controls.Add(this.label1);
            this.tabTeken.Controls.Add(this.Histogram);
            this.tabTeken.Controls.Add(this.dataGridView1);
            this.tabTeken.Controls.Add(this.Txt_Cat);
            this.tabTeken.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabTeken.Location = new System.Drawing.Point(4, 25);
            this.tabTeken.Name = "tabTeken";
            this.tabTeken.Padding = new System.Windows.Forms.Padding(3);
            this.tabTeken.Size = new System.Drawing.Size(1896, 871);
            this.tabTeken.TabIndex = 0;
            this.tabTeken.Text = "כלל התקנים";
            // 
            // Btn_UpdateFactor
            // 
            this.Btn_UpdateFactor.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Btn_UpdateFactor.Location = new System.Drawing.Point(1786, 695);
            this.Btn_UpdateFactor.Name = "Btn_UpdateFactor";
            this.Btn_UpdateFactor.Size = new System.Drawing.Size(99, 56);
            this.Btn_UpdateFactor.TabIndex = 261;
            this.Btn_UpdateFactor.Text = "עדכן פקטור";
            this.Btn_UpdateFactor.UseVisualStyleBackColor = true;
            this.Btn_UpdateFactor.Click += new System.EventHandler(this.Btn_UpdateFactor_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.label13.Location = new System.Drawing.Point(1782, 624);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 19);
            this.label13.TabIndex = 260;
            this.label13.Text = "פקטור תכנון";
            // 
            // Txt_Factor
            // 
            this.Txt_Factor.BackColor = System.Drawing.Color.PaleGreen;
            this.Txt_Factor.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.Txt_Factor.Location = new System.Drawing.Point(1785, 662);
            this.Txt_Factor.Name = "Txt_Factor";
            this.Txt_Factor.Size = new System.Drawing.Size(100, 27);
            this.Txt_Factor.TabIndex = 259;
            this.Txt_Factor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Txt_JumboMain
            // 
            this.Txt_JumboMain.BackColor = System.Drawing.Color.NavajoWhite;
            this.Txt_JumboMain.Font = new System.Drawing.Font("Poor Richard", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_JumboMain.Location = new System.Drawing.Point(1294, 17);
            this.Txt_JumboMain.Name = "Txt_JumboMain";
            this.Txt_JumboMain.Size = new System.Drawing.Size(268, 29);
            this.Txt_JumboMain.TabIndex = 258;
            this.Txt_JumboMain.Text = " תקן ירוק עבור ג\'מבו וטרקטור";
            this.Txt_JumboMain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_JumboMain.Visible = false;
            // 
            // Txt_TiresInShfitMain
            // 
            this.Txt_TiresInShfitMain.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Txt_TiresInShfitMain.Font = new System.Drawing.Font("Poor Richard", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Txt_TiresInShfitMain.Location = new System.Drawing.Point(1563, 17);
            this.Txt_TiresInShfitMain.Name = "Txt_TiresInShfitMain";
            this.Txt_TiresInShfitMain.Size = new System.Drawing.Size(276, 29);
            this.Txt_TiresInShfitMain.TabIndex = 257;
            this.Txt_TiresInShfitMain.Text = " מס\' צמיגים במשמרת";
            this.Txt_TiresInShfitMain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_TiresInShfitMain.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label12.Location = new System.Drawing.Point(539, 0);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label12.Size = new System.Drawing.Size(148, 19);
            this.label12.TabIndex = 254;
            this.label12.Text = "הזן מכונה לחיפוש:";
            // 
            // Txt_Mach
            // 
            this.Txt_Mach.Location = new System.Drawing.Point(577, 24);
            this.Txt_Mach.Name = "Txt_Mach";
            this.Txt_Mach.Size = new System.Drawing.Size(70, 22);
            this.Txt_Mach.TabIndex = 253;
            this.Txt_Mach.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Mach.Visible = false;
            this.Txt_Mach.TextChanged += new System.EventHandler(this.Txt_Mach_TextChanged);
            // 
            // Btn_UpdateNorm
            // 
            this.Btn_UpdateNorm.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_UpdateNorm.Location = new System.Drawing.Point(1594, 798);
            this.Btn_UpdateNorm.Name = "Btn_UpdateNorm";
            this.Btn_UpdateNorm.Size = new System.Drawing.Size(215, 35);
            this.Btn_UpdateNorm.TabIndex = 252;
            this.Btn_UpdateNorm.Text = "עדכן תקנים";
            this.Btn_UpdateNorm.UseVisualStyleBackColor = true;
            this.Btn_UpdateNorm.Visible = false;
            this.Btn_UpdateNorm.Click += new System.EventHandler(this.Btn_UpdateNorm_Click);
            // 
            // Lbl_NormCount
            // 
            this.Lbl_NormCount.AutoSize = true;
            this.Lbl_NormCount.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_NormCount.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_NormCount.ForeColor = System.Drawing.Color.Black;
            this.Lbl_NormCount.Location = new System.Drawing.Point(1694, 488);
            this.Lbl_NormCount.Name = "Lbl_NormCount";
            this.Lbl_NormCount.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_NormCount.Size = new System.Drawing.Size(0, 23);
            this.Lbl_NormCount.TabIndex = 251;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(68, 1);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(140, 19);
            this.label1.TabIndex = 4;
            this.label1.Text = "הזן מק\'ט לחיפוש:";
            // 
            // Histogram
            // 
            chartArea1.Name = "ChartArea1";
            this.Histogram.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.Histogram.Legends.Add(legend1);
            this.Histogram.Location = new System.Drawing.Point(8, 488);
            this.Histogram.Name = "Histogram";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Tires";
            series2.ChartArea = "ChartArea1";
            series2.Color = System.Drawing.Color.Red;
            series2.Legend = "Legend1";
            series2.Name = "Norm";
            this.Histogram.Series.Add(series1);
            this.Histogram.Series.Add(series2);
            this.Histogram.Size = new System.Drawing.Size(1491, 375);
            this.Histogram.TabIndex = 3;
            this.Histogram.Text = "chart1";
            // 
            // tabEdit
            // 
            this.tabEdit.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabEdit.Controls.Add(this.Txt_JumboNorm);
            this.tabEdit.Controls.Add(this.Txt_TiresInShift);
            this.tabEdit.Controls.Add(this.Lbl_CountGaps);
            this.tabEdit.Controls.Add(this.label10);
            this.tabEdit.Controls.Add(this.Btn_Update);
            this.tabEdit.Controls.Add(this.label8);
            this.tabEdit.Controls.Add(this.Lbl_Gaps);
            this.tabEdit.Controls.Add(this.Txt_Percentage);
            this.tabEdit.Controls.Add(this.Btn_Gaps);
            this.tabEdit.Controls.Add(this.DGV_Gaps);
            this.tabEdit.Location = new System.Drawing.Point(4, 25);
            this.tabEdit.Name = "tabEdit";
            this.tabEdit.Padding = new System.Windows.Forms.Padding(3);
            this.tabEdit.Size = new System.Drawing.Size(1896, 871);
            this.tabEdit.TabIndex = 1;
            this.tabEdit.Text = "פערי תקן";
            // 
            // Txt_JumboNorm
            // 
            this.Txt_JumboNorm.BackColor = System.Drawing.Color.NavajoWhite;
            this.Txt_JumboNorm.Font = new System.Drawing.Font("Poor Richard", 14.25F, System.Drawing.FontStyle.Bold);
            this.Txt_JumboNorm.Location = new System.Drawing.Point(1176, 141);
            this.Txt_JumboNorm.Name = "Txt_JumboNorm";
            this.Txt_JumboNorm.Size = new System.Drawing.Size(211, 29);
            this.Txt_JumboNorm.TabIndex = 260;
            this.Txt_JumboNorm.Text = "תקן ירוק ג\'מבו וטרקטור";
            this.Txt_JumboNorm.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_JumboNorm.Visible = false;
            // 
            // Txt_TiresInShift
            // 
            this.Txt_TiresInShift.BackColor = System.Drawing.Color.LightSkyBlue;
            this.Txt_TiresInShift.Font = new System.Drawing.Font("Poor Richard", 14.25F, System.Drawing.FontStyle.Bold);
            this.Txt_TiresInShift.Location = new System.Drawing.Point(1386, 141);
            this.Txt_TiresInShift.Name = "Txt_TiresInShift";
            this.Txt_TiresInShift.Size = new System.Drawing.Size(211, 29);
            this.Txt_TiresInShift.TabIndex = 259;
            this.Txt_TiresInShift.Text = "  מס\' צמיגים במשמרת ";
            this.Txt_TiresInShift.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_TiresInShift.Visible = false;
            // 
            // Lbl_CountGaps
            // 
            this.Lbl_CountGaps.AutoSize = true;
            this.Lbl_CountGaps.BackColor = System.Drawing.Color.Transparent;
            this.Lbl_CountGaps.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_CountGaps.ForeColor = System.Drawing.Color.Black;
            this.Lbl_CountGaps.Location = new System.Drawing.Point(1411, 656);
            this.Lbl_CountGaps.Name = "Lbl_CountGaps";
            this.Lbl_CountGaps.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_CountGaps.Size = new System.Drawing.Size(0, 23);
            this.Lbl_CountGaps.TabIndex = 255;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(948, 424);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(0, 23);
            this.label10.TabIndex = 254;
            // 
            // Btn_Update
            // 
            this.Btn_Update.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Update.Location = new System.Drawing.Point(1401, 702);
            this.Btn_Update.Name = "Btn_Update";
            this.Btn_Update.Size = new System.Drawing.Size(215, 35);
            this.Btn_Update.TabIndex = 253;
            this.Btn_Update.Text = "עדכן תקנים";
            this.Btn_Update.UseVisualStyleBackColor = true;
            this.Btn_Update.Visible = false;
            this.Btn_Update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(1752, 163);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 16);
            this.label8.TabIndex = 10;
            this.label8.Text = "%";
            // 
            // Lbl_Gaps
            // 
            this.Lbl_Gaps.AutoSize = true;
            this.Lbl_Gaps.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Gaps.Location = new System.Drawing.Point(1779, 163);
            this.Lbl_Gaps.Name = "Lbl_Gaps";
            this.Lbl_Gaps.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_Gaps.Size = new System.Drawing.Size(109, 16);
            this.Lbl_Gaps.TabIndex = 9;
            this.Lbl_Gaps.Text = "הצג פערים מעל:";
            // 
            // Txt_Percentage
            // 
            this.Txt_Percentage.BackColor = System.Drawing.Color.PaleGreen;
            this.Txt_Percentage.Location = new System.Drawing.Point(1705, 159);
            this.Txt_Percentage.Name = "Txt_Percentage";
            this.Txt_Percentage.Size = new System.Drawing.Size(48, 22);
            this.Txt_Percentage.TabIndex = 8;
            this.Txt_Percentage.Text = "20";
            this.Txt_Percentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Percentage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Percentage_KeyPress);
            // 
            // Btn_Gaps
            // 
            this.Btn_Gaps.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Gaps.Location = new System.Drawing.Point(1401, 60);
            this.Btn_Gaps.Name = "Btn_Gaps";
            this.Btn_Gaps.Size = new System.Drawing.Size(215, 35);
            this.Btn_Gaps.TabIndex = 7;
            this.Btn_Gaps.Text = "הצג פערי תקנים";
            this.Btn_Gaps.UseVisualStyleBackColor = true;
            this.Btn_Gaps.Click += new System.EventHandler(this.Btn_Gaps_Click);
            // 
            // DGV_Gaps
            // 
            this.DGV_Gaps.AllowUserToAddRows = false;
            this.DGV_Gaps.AllowUserToDeleteRows = false;
            this.DGV_Gaps.AllowUserToOrderColumns = true;
            this.DGV_Gaps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Gaps.Location = new System.Drawing.Point(65, 170);
            this.DGV_Gaps.Name = "DGV_Gaps";
            this.DGV_Gaps.ReadOnly = true;
            this.DGV_Gaps.Size = new System.Drawing.Size(1551, 460);
            this.DGV_Gaps.TabIndex = 5;
            this.DGV_Gaps.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DGV_Gaps_CellBeginEdit);
            this.DGV_Gaps.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_Gaps_CellEndEdit);
            this.DGV_Gaps.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGV_Gaps_CellValidating);
            // 
            // tabNew
            // 
            this.tabNew.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tabNew.Controls.Add(this.Lbl_Prog);
            this.tabNew.Controls.Add(this.PB_NewItems);
            this.tabNew.Controls.Add(this.label6);
            this.tabNew.Controls.Add(this.label2);
            this.tabNew.Controls.Add(this.label5);
            this.tabNew.Controls.Add(this.Grp_Jumbo);
            this.tabNew.Controls.Add(this.DGV_Weightings);
            this.tabNew.Controls.Add(this.label3);
            this.tabNew.Controls.Add(this.Btn_NewNorm);
            this.tabNew.Controls.Add(this.DGV_Similar);
            this.tabNew.Controls.Add(this.DGV_New);
            this.tabNew.Controls.Add(this.btnNew);
            this.tabNew.Location = new System.Drawing.Point(4, 25);
            this.tabNew.Name = "tabNew";
            this.tabNew.Size = new System.Drawing.Size(1896, 871);
            this.tabNew.TabIndex = 2;
            this.tabNew.Text = "הוסף תקן חדש";
            // 
            // Lbl_Prog
            // 
            this.Lbl_Prog.AutoSize = true;
            this.Lbl_Prog.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Prog.Location = new System.Drawing.Point(273, 104);
            this.Lbl_Prog.Name = "Lbl_Prog";
            this.Lbl_Prog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_Prog.Size = new System.Drawing.Size(235, 18);
            this.Lbl_Prog.TabIndex = 15;
            this.Lbl_Prog.Text = "אנא המתן לאחזור כל הרשומות...";
            this.Lbl_Prog.Visible = false;
            // 
            // PB_NewItems
            // 
            this.PB_NewItems.Location = new System.Drawing.Point(43, 99);
            this.PB_NewItems.Name = "PB_NewItems";
            this.PB_NewItems.Size = new System.Drawing.Size(215, 23);
            this.PB_NewItems.TabIndex = 14;
            this.PB_NewItems.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label6.Location = new System.Drawing.Point(1342, 480);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 23);
            this.label6.TabIndex = 13;
            this.label6.Text = "תקנים";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(354, 480);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 23);
            this.label2.TabIndex = 12;
            this.label2.Text = "שקילות ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label5.Location = new System.Drawing.Point(822, 441);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 29);
            this.label5.TabIndex = 11;
            this.label5.Text = "פרטי מק\"טים בני דודים";
            // 
            // Grp_Jumbo
            // 
            this.Grp_Jumbo.Controls.Add(this.groupBox1);
            this.Grp_Jumbo.Controls.Add(this.label9);
            this.Grp_Jumbo.Controls.Add(this.Txt_Green);
            this.Grp_Jumbo.Controls.Add(this.Txt_CatNum);
            this.Grp_Jumbo.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Grp_Jumbo.Location = new System.Drawing.Point(1382, 129);
            this.Grp_Jumbo.Name = "Grp_Jumbo";
            this.Grp_Jumbo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Grp_Jumbo.Size = new System.Drawing.Size(482, 278);
            this.Grp_Jumbo.TabIndex = 10;
            this.Grp_Jumbo.TabStop = false;
            this.Grp_Jumbo.Text = "מחשבון טרקטור / ג\'מבו";
            this.Grp_Jumbo.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.Txt_Minutes);
            this.groupBox1.Controls.Add(this.Txt_Calc);
            this.groupBox1.Controls.Add(this.Lbl_Tires);
            this.groupBox1.Location = new System.Drawing.Point(6, 122);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(470, 150);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(258, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 19);
            this.label11.TabIndex = 6;
            this.label11.Text = "דק\' נטו:";
            // 
            // Txt_Minutes
            // 
            this.Txt_Minutes.BackColor = System.Drawing.Color.PaleGreen;
            this.Txt_Minutes.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Minutes.Location = new System.Drawing.Point(176, 115);
            this.Txt_Minutes.Name = "Txt_Minutes";
            this.Txt_Minutes.ReadOnly = true;
            this.Txt_Minutes.Size = new System.Drawing.Size(76, 30);
            this.Txt_Minutes.TabIndex = 5;
            this.Txt_Minutes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Txt_Calc
            // 
            this.Txt_Calc.BackColor = System.Drawing.Color.Plum;
            this.Txt_Calc.Font = new System.Drawing.Font("Tahoma", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Calc.Location = new System.Drawing.Point(176, 63);
            this.Txt_Calc.Name = "Txt_Calc";
            this.Txt_Calc.ReadOnly = true;
            this.Txt_Calc.Size = new System.Drawing.Size(76, 43);
            this.Txt_Calc.TabIndex = 4;
            this.Txt_Calc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Calc.TextChanged += new System.EventHandler(this.Txt_Calc_TextChanged);
            // 
            // Lbl_Tires
            // 
            this.Lbl_Tires.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Lbl_Tires.AutoSize = true;
            this.Lbl_Tires.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Tires.Location = new System.Drawing.Point(51, 24);
            this.Lbl_Tires.Name = "Lbl_Tires";
            this.Lbl_Tires.Size = new System.Drawing.Size(236, 23);
            this.Lbl_Tires.TabIndex = 3;
            this.Lbl_Tires.Text = "מספר צמיגים למשמרת:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(157, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(270, 19);
            this.label9.TabIndex = 2;
            this.label9.Text = "הזן מספר צמיגים ירוקים במשמרת:";
            // 
            // Txt_Green
            // 
            this.Txt_Green.BackColor = System.Drawing.Color.Yellow;
            this.Txt_Green.Font = new System.Drawing.Font("Tahoma", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Green.Location = new System.Drawing.Point(61, 80);
            this.Txt_Green.Name = "Txt_Green";
            this.Txt_Green.Size = new System.Drawing.Size(76, 40);
            this.Txt_Green.TabIndex = 1;
            this.Txt_Green.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_Green.TextChanged += new System.EventHandler(this.Txt_Green_TextChanged);
            this.Txt_Green.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_Green_KeyPress);
            // 
            // Txt_CatNum
            // 
            this.Txt_CatNum.Location = new System.Drawing.Point(134, 36);
            this.Txt_CatNum.Name = "Txt_CatNum";
            this.Txt_CatNum.ReadOnly = true;
            this.Txt_CatNum.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Txt_CatNum.Size = new System.Drawing.Size(192, 27);
            this.Txt_CatNum.TabIndex = 0;
            this.Txt_CatNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // DGV_Weightings
            // 
            this.DGV_Weightings.AllowUserToAddRows = false;
            this.DGV_Weightings.AllowUserToDeleteRows = false;
            this.DGV_Weightings.AllowUserToOrderColumns = true;
            this.DGV_Weightings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Weightings.Location = new System.Drawing.Point(43, 517);
            this.DGV_Weightings.Name = "DGV_Weightings";
            this.DGV_Weightings.ReadOnly = true;
            this.DGV_Weightings.Size = new System.Drawing.Size(1038, 152);
            this.DGV_Weightings.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(840, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(229, 29);
            this.label3.TabIndex = 8;
            this.label3.Text = "הוספת מק\"ט חדש";
            // 
            // Btn_NewNorm
            // 
            this.Btn_NewNorm.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_NewNorm.Location = new System.Drawing.Point(827, 791);
            this.Btn_NewNorm.Name = "Btn_NewNorm";
            this.Btn_NewNorm.Size = new System.Drawing.Size(215, 35);
            this.Btn_NewNorm.TabIndex = 6;
            this.Btn_NewNorm.Text = "הוסף תקנים חדשים";
            this.Btn_NewNorm.UseVisualStyleBackColor = true;
            this.Btn_NewNorm.Visible = false;
            this.Btn_NewNorm.Click += new System.EventHandler(this.Btn_NewNorm_Click);
            // 
            // DGV_Similar
            // 
            this.DGV_Similar.AllowUserToAddRows = false;
            this.DGV_Similar.AllowUserToOrderColumns = true;
            this.DGV_Similar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_Similar.Location = new System.Drawing.Point(1146, 517);
            this.DGV_Similar.Name = "DGV_Similar";
            this.DGV_Similar.ReadOnly = true;
            this.DGV_Similar.Size = new System.Drawing.Size(735, 152);
            this.DGV_Similar.TabIndex = 5;
            // 
            // DGV_New
            // 
            this.DGV_New.AllowUserToAddRows = false;
            this.DGV_New.AllowUserToDeleteRows = false;
            this.DGV_New.AllowUserToOrderColumns = true;
            this.DGV_New.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_New.Location = new System.Drawing.Point(43, 129);
            this.DGV_New.Name = "DGV_New";
            this.DGV_New.ReadOnly = true;
            this.DGV_New.Size = new System.Drawing.Size(1251, 287);
            this.DGV_New.TabIndex = 4;
            this.DGV_New.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_New_CellClick);
            this.DGV_New.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_New_CellContentDoubleClick);
            this.DGV_New.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_New_CellEndEdit);
            this.DGV_New.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.DGV_New_CellValidating);
            // 
            // btnNew
            // 
            this.btnNew.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.btnNew.Location = new System.Drawing.Point(43, 60);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(215, 35);
            this.btnNew.TabIndex = 3;
            this.btnNew.Text = "הבא מק\"טים חדשים";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1904, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(36, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // manageToolStripMenuItem
            // 
            this.manageToolStripMenuItem.Name = "manageToolStripMenuItem";
            this.manageToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.manageToolStripMenuItem.Text = "Manage";
            this.manageToolStripMenuItem.Click += new System.EventHandler(this.manageToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label7.Location = new System.Drawing.Point(1474, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(415, 16);
            this.label7.TabIndex = 249;
            this.label7.Text = "Building Norms Management Tool - Created By Ran Oichman  -  V1.0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.ForeColor = System.Drawing.Color.LightBlue;
            this.label4.Location = new System.Drawing.Point(881, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(304, 42);
            this.label4.TabIndex = 250;
            this.label4.Text = "ניהול תקני בנייה";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BuildingNorms.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(1904, 1042);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form2";
            this.Text = "Building Norms";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabTeken.ResumeLayout(false);
            this.tabTeken.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Histogram)).EndInit();
            this.tabEdit.ResumeLayout(false);
            this.tabEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Gaps)).EndInit();
            this.tabNew.ResumeLayout(false);
            this.tabNew.PerformLayout();
            this.Grp_Jumbo.ResumeLayout(false);
            this.Grp_Jumbo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Weightings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_Similar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_New)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox Txt_Cat;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabTeken;
        private System.Windows.Forms.TabPage tabEdit;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.TabPage tabNew;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart Histogram;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_NewNorm;
        private System.Windows.Forms.DataGridView DGV_Similar;
        private System.Windows.Forms.DataGridView DGV_New;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Lbl_NormCount;
        private System.Windows.Forms.ToolStripMenuItem manageToolStripMenuItem;
        private System.Windows.Forms.DataGridView DGV_Gaps;
        private System.Windows.Forms.Button Btn_Gaps;
        private System.Windows.Forms.DataGridView DGV_Weightings;
        private System.Windows.Forms.GroupBox Grp_Jumbo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ProgressBar PB_NewItems;
        private System.Windows.Forms.Label Lbl_Prog;
        private System.Windows.Forms.Button Btn_UpdateNorm;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label Lbl_Gaps;
        private System.Windows.Forms.TextBox Txt_Percentage;
        private System.Windows.Forms.Button Btn_Update;
        private System.Windows.Forms.TextBox Txt_Green;
        private System.Windows.Forms.TextBox Txt_CatNum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Txt_Calc;
        private System.Windows.Forms.Label Lbl_Tires;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox Txt_Minutes;
        private System.Windows.Forms.Label Lbl_CountGaps;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox Txt_Mach;
        private System.Windows.Forms.TextBox Txt_JumboNorm;
        private System.Windows.Forms.TextBox Txt_TiresInShift;
        private System.Windows.Forms.TextBox Txt_JumboMain;
        private System.Windows.Forms.TextBox Txt_TiresInShfitMain;
        private System.Windows.Forms.Button Btn_UpdateFactor;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox Txt_Factor;
    }
}