﻿using System;
using System.Windows.Forms;
using cwbx;
using System.Data;
using System.Data.Odbc;
using System.Data.Common;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;
using System.Data.SqlClient;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data.OleDb;

namespace BuildingNorms
{
    public partial class Form2 : Form
    {
        DataTable DTAllNorms = new DataTable(); // כל המקטים בבסיס הנתונים
        DataTable DTWeightingNorms = new DataTable(); // טבלת סיכום תקן וכמות צמיגים שנבנתה בתקן
        Dictionary<int, int> rowsToUpd = new Dictionary<int, int>();// השורות הרלוונטיות לעדכון בסיס הנתונים
        Task TgetNorms;
        // משתני חלוקת התקן בין שני השלבים : קרקס 60% ושלב ב 40%
        static public Tuple<double, double> X = Form1.GetRelations();
        static public double level1 = X.Item1;
        static public double level0 = X.Item2;
        public static double Factor = Form1.GetFactor();

        // Tests
        BindingSource bindingSource1 = new BindingSource();
        OleDbConnection con = new OleDbConnection();
        OleDbDataAdapter dataAdapter = new OleDbDataAdapter();
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // Load accumulated Norms
            TgetNorms = new Task(() => LoadWeightingNorms());
            TgetNorms.Start();
            Txt_Factor.Text = Factor.ToString();

            LoadData();
            StyleDGV();
        }

        public void GetData(string selectCommand)
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            con = new OleDbConnection("Provider =IBMDA400;Data Source=172.16.1.158;User ID=as400;Password=as400");

            // Create a command builder to generate SQL update, insert, and
            // delete commands based on selectCommand. These are used to
            // update the database.
            //OleDbCommand commandBuilder = new OleDbCommand(selectCommand, con);
            dataAdapter = new OleDbDataAdapter(selectCommand, con);
            dataAdapter.Fill(DT);
            bindingSource1.DataSource = DT;
        }

        public void LoadData()
        {
            DBService dbs = new DBService();
            DbServiceSQL dbSql = new DbServiceSQL();

            DTAllNorms.Clear();
            DTAllNorms.PrimaryKey = new DataColumn[] { DTAllNorms.Columns["rmprod"], DTAllNorms.Columns["MOFRE1"] };
            DataTable Count = new DataTable();
            DataTable NewDt = new DataTable();
            NewDt.Columns.Add("prod");
            NewDt.Columns.Add("grp");
            NewDt.Columns.Add("num");
            //NewDt.Columns.Add("percentile");
            NewDt.PrimaryKey = new DataColumn[] { NewDt.Columns["prod"], NewDt.Columns["grp"] };

            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            // set it to false if not needed
            dataGridView1.RowHeadersVisible = false;

            string StrSql = @"select WDEPT,RMPROD,CASE WHEN iclas ='L' THEN 'קרקס' ELSE CASE WHEN MOFRE2='1' THEN 'שלב ב' ELSE  'ירוק' END END AS Class,INSIZ,right(IDRAW,9) as IDRAW,RMMACI as mac,CAST(ROUND(1440 / RMTKN*0.72,0) AS decimal(10,2)) as RMTKN,CASE WHEN RMMACI = '78' THEN CASE WHEN INDOR = 'D' THEN 2 ELSE  RMOPR END ELSE RMOPR END AS RMOPR,MOFRE1,0 as num,CASE WHEN RMOPR = 2 THEN CAST(ROUND(365 / (1440 / RMTKN*0.72 * 1.4),0)   AS decimal(10,0)) ELSE CAST(ROUND(365 / (1440 / RMTKN*0.72 ),0)  AS decimal(10,0)) END as One_Worker, CASE WHEN RMOPR < 1.5 AND RMOPR <> 0 THEN CAST(ROUND((365 / (1440 / RMTKN*0.72 )) * 1.4 ,0) AS decimal(10,0)) ELSE CAST(ROUND((365 / (1440 / RMTKN*0.72)),0) AS decimal(10,0)) END  as Two_Workers, 
                        Case 
		               	WHEN RMMACI = '12' OR RMMACI = '16' OR RMMACI = '73' OR RMMACI = '75' THEN CASE WHEN substring(RMPROD,10,1)= '0' THEN 2.5 * CAST(ROUND((1440 / RMTKN*0.72),1) AS decimal(10,0)) ELSE 1.6 * CAST(ROUND((1440 / RMTKN*0.72),1) AS decimal(10,0)) END ELSE  CAST(ROUND((1440 / RMTKN*0.72),1) AS decimal(10,0)) 
                                END as Jumbo, 0 as TiresInShift
                                , CAST(ROUND(300/(1440/RMTKN*0.72) * 1.08,0) AS decimal(10,0)) as NightShift,CAST(ROUND(365/(1440/RMTKN*0.72) * 1.08,0) AS decimal(10,0)) as DayShift,CAST(ROUND(520/(1440/RMTKN*0.72) * 1.08,0) AS decimal(10,0)) as Weekend,RMJOB as LastModifier,
                                CASE WHEN RMOPR <> 2 THEN
                                CASE WHEN MOSET=0 THEN 0 ELSE 
                                FLOOR((CAST(ROUND((365 / (1440 / RMTKN*0.72 )),0) AS decimal(10,0)
                                ) - MOHIT) / nullif(MOHIT,0) * NULLIF(MOSET,0) / CAST(ROUND((365 / (1440 / RMTKN*0.72 )),0) AS decimal(10,0)) + 0.5) END ELSE 0 END  as AddedTime,MOFRE3
                                from bpcsfali.FRTML01
                                left join BPCSFV30.iiml01 ON RMPROD=IPROD
                                left join BPCSFALI.iimnl01 ON substring(RMPROD,1,8)=substring(INPROD,1,8)
                                left join BPCSFALI.LMHOL01 ON MOMACH=RMMACI and  MOWRKC=RMWRKC
                                left join BPCSFV30.LWKL01 ON WWRKC=MOWRKC 
                                WHERE substring(iclas,1,1) in('M','N','L') and RMWRKC BETWEEN 750000 and 759999 
                                group by WDEPT,RMPROD,iclas,MOFRE2,IDRAW,IDESC,RMMACI,RMTKN,RMTKN2,MOFRE1,RMOPR,MOFRE1,INSIZ,RMJOB,MOSET, MOHIT,INDOR,MOFRE3";
            #region
            //StrSql = @"select WDEPT,RMPROD,CASE WHEN iclas ='L' THEN 'קרקס' ELSE CASE WHEN MOFRE2='1' THEN 'שלב ב' ELSE  'ירוק' END END AS Class,INSIZ,right(IDRAW,9) as IDRAW,RMMACI as mac,CAST(ROUND(1440 / RMTKN*0.72,0) AS decimal(10,2)) as RMTKN,RMOPR,MOFRE1,0 as num,CASE WHEN RMOPR = 2 THEN CAST(ROUND(365 / (1440 / RMTKN*0.72 * 1.4),0)   AS decimal(10,0)) ELSE CAST(ROUND(365 / (1440 / RMTKN*0.72 ),0)  AS decimal(10,0)) END as One_Worker, CASE WHEN RMOPR < 1.5 AND RMOPR <> 0 THEN CAST(ROUND((365 / (1440 / RMTKN*0.72 )) * 1.4 ,0) AS decimal(10,0)) ELSE CAST(ROUND((365 / (1440 / RMTKN*0.72)),0) AS decimal(10,0)) END  as Two_Workers, 
            //            Case 
		          //     	WHEN RMMACI = '12' OR RMMACI = '16' OR RMMACI = '73' OR RMMACI = '75' THEN CASE WHEN substring(RMPROD,10,1)= '0' THEN 2.5 * CAST(ROUND((1440 / RMTKN*0.72),1) AS decimal(10,0)) ELSE 1.6 * CAST(ROUND((1440 / RMTKN*0.72),1) AS decimal(10,0)) END ELSE  CAST(ROUND((1440 / RMTKN*0.72),1) AS decimal(10,0)) 
            //                    END as Jumbo, 0 as TiresInShift
            //                    , CAST(ROUND(300/(1440/RMTKN*0.72) * 1.08,0) AS decimal(10,0)) as NightShift,CAST(ROUND(365/(1440/RMTKN*0.72) * 1.08,0) AS decimal(10,0)) as DayShift,CAST(ROUND(520/(1440/RMTKN*0.72) * 1.08,0) AS decimal(10,0)) as Weekend,RMJOB as LastModifier
            //                    from bpcsfali.FRTML01
            //                    left join BPCSFV30.iiml01 ON RMPROD=IPROD
            //                    left join BPCSFALI.iimnl01 ON substring(RMPROD,1,8)=substring(INPROD,1,8)
            //                    left join BPCSFALI.LMHOL01 ON MOMACH=RMMACI and  MOWRKC=RMWRKC
            //                    left join BPCSFV30.LWKL01 ON WWRKC=MOWRKC 
            //                    WHERE substring(iclas,1,1) in('M','N','L') and RMWRKC BETWEEN 750000 and 759999 
            //                    group by WDEPT,RMPROD,iclas,MOFRE2,IDRAW,IDESC,RMMACI,RMTKN,RMTKN2,MOFRE1,RMOPR,MOFRE1,INSIZ,RMJOB";


            //StrSql = @"select RMPROD,INSIZ,right(IDRAW,9) as IDRAW,max(RMMACI)            
            //               as mac,max(CAST(1440 / RMTKN*0.72 AS decimal(10,2))) as            
            //               RMTKN,max(RMOPR) as RMOPR,max(MOFRE1) as MOFRE1,'' as num                             
            //               from bpcsfali.FRTML01                                              
            //               left join BPCSFV30.iiml01 ON RMPROD=IPROD                          
            //               left join BPCSFALI.iimnl01 ON                                      
            //               substring(RMPROD,1,8)=substring(INPROD,1,8)                        
            //               left join BPCSFALI.LMHOL01 ON MOMACH=RMMACI and  MOWRKC=RMWRKC     
            //               WHERE substring(iclas,1,1) in('M','N','L')                         
            //               and RMWRKC BETWEEN 750000 and 759999                               
            //               group by                                                           
            //               RMPROD,IDRAW,IDESC,MOFRE1,RMOPR,INSIZ ";
            #endregion
            DTAllNorms = dbs.executeSelectQueryNoParam(StrSql); // כלל המק"טים
            // יצירת טבלה נוספת שתשרת את המסך הראשי
            var filtered = DTAllNorms.AsEnumerable().GroupBy(r => new { Col1 = r["RMPROD"], Col2 = r["IDRAW"], Col4 = r["MOFRE1"], Col5 = r["RMOPR"], Col6 = r["INSIZ"] }).Select(g => g.OrderBy(r => r["RMPROD"]).First()).CopyToDataTable();
            DataTable displayedNorms = filtered;

            // הבאת כמויות השקילה מהסקל
            StrSql = "SELECT * FROM [dbo].[View_CountNorm] ";
            Count = dbSql.executeSelectQueryNoParam(StrSql);
            try
            {
                // שליפת נתונים מקבצי השקילה ב סקל
                var result = from dataRows1 in Count.AsEnumerable()
                             join dataRows2 in displayedNorms.AsEnumerable()
                             on new { prod = dataRows1.Field<string>("Cat_Number"), grp = dataRows1.Field<string>("grp") } equals new { prod = dataRows2.Field<string>("rmprod"), grp = dataRows2.Field<string>("MOFRE1") } into lj
                             from r in lj.DefaultIfEmpty()
                                 //select dataRows1;
                             select new
                             {
                                 prod = dataRows1.Field<string>("Cat_Number"),
                                 grp = dataRows1.Field<string>("grp"),
                                 num = dataRows1.Field<int>("num")
                                 //percentile = dataRows1.Field<double>("percentile"),
                             };

                var distinctValues = result.AsEnumerable()
                        .Select(row => new
                        {
                            prod = row.prod,
                            grp = row.grp,
                            num = row.num
                            //percentile = row.percentile
                        })
                        .Distinct();

                foreach (var item in distinctValues)
                {
                    try
                    {
                        NewDt.Rows.Add(item.prod, item.grp, item.num);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                }
                foreach (DataRow row in displayedNorms.Rows)
                {
                    DataRow[] norms = NewDt.Select("prod ='" + row["rmprod"] + "' AND grp='" + row["MOFRE1"] + "' ");
                    foreach (DataRow R in norms)
                    {
                        row["num"] = R["num"]; // הזנת מס שקילות
                    }
                    row["TiresInShift"] = 365 / double.Parse(row["JUMBO"].ToString());  // הזנת מס צמיגים ירוקים למשמרת
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            dataGridView1.Invoke((MethodInvoker)delegate () { dataGridView1.DataSource = displayedNorms; });
            dataGridView1.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            // set it to false if not needed
            dataGridView1.RowHeadersVisible = false;
        }

        // מחזירה את טבלת 
        // View_WeightingNorms
        public void LoadWeightingNorms()
        {
            DbServiceSQL dbs = new DbServiceSQL();
            string StrSql = @"SELECT * FROM View_Norm_Number ";
            DTWeightingNorms = dbs.executeSelectQueryNoParam(StrSql);
        }

        public void StyleDGV()
        {
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                //column.SortMode = DataGridViewColumnSortMode.Automatic;
                column.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                if (column.DataPropertyName == "RMPROD")
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                else
                {
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                }

                if (column.DataPropertyName == "JUMBO" || column.DataPropertyName == "TIRESINSHIFT")
                {
                    column.DefaultCellStyle.BackColor = Color.NavajoWhite;
                }

                if (column.DataPropertyName == "DAYSHIFT" || column.DataPropertyName == "NIGHTSHIFT" || column.DataPropertyName == "WEEKEND")
                {
                    column.DefaultCellStyle.BackColor = Color.LightSkyBlue;
                }

                if (column.DataPropertyName == "RMPROD")
                {
                    column.DefaultCellStyle.ForeColor = Color.Red;
                }

            }

            tabControl1.RightToLeftLayout = true;
            dataGridView1.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

            dataGridView1.ReadOnly = false;
            foreach (DataGridViewColumn c in dataGridView1.Columns)
            {
                if (c.DataPropertyName == "RMPROD")
                {
                    c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
                c.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                c.ReadOnly = true;
                if (c.DataPropertyName == "RMTKN" || c.DataPropertyName == "RMTKN2")
                {
                    c.ReadOnly = false;  // editable
                    c.DefaultCellStyle.BackColor = Color.PaleGreen;
                }
                c.DisplayIndex = c.Index;
            }

            //foreach (DataGridViewRow row in dataGridView1.Rows)
            //{
            //    if (row.Cells["MOFRE3"].Value.ToString() == "1")
            //    {
            //        row.Cells["RMTKN"].Style.BackColor = Color.PaleVioletRed;
            //    }
            //}

            // עיצוב כותרת
            dataGridView1.Columns["RMPROD"].HeaderText = "מק'ט";
            dataGridView1.Columns["RMPROD"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns["CLASS"].HeaderText = "תאור שלב";
            dataGridView1.Columns["INSIZ"].HeaderText = "גודל צמיג";
            dataGridView1.Columns["INSIZ"].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns["IDRAW"].HeaderText = "מפרט";
            dataGridView1.Columns["mac"].HeaderText = "מכונה";
            dataGridView1.Columns["RMTKN"].HeaderText = "זמן נטו לצמיג";
            dataGridView1.Columns["RMOPR"].HeaderText = "מס עובדים";
            dataGridView1.Columns["MOFRE1"].HeaderText = "קבוצה";
            dataGridView1.Columns["num"].HeaderText = "מס' שקילות";
            dataGridView1.Columns["One_Worker"].HeaderText = "מס' צמיגים לעובד אחד";
            dataGridView1.Columns["Two_Workers"].HeaderText = "מס' צמיגים לשני עובדים";
            dataGridView1.Columns["JUMBO"].HeaderText = "זמן נטו לצמיג ירוק";
            dataGridView1.Columns["TIRESINSHIFT"].HeaderText = "מס' צמיגים במשמרת";
            dataGridView1.Columns["NightShift"].HeaderText = "משמרת 7 שעות";
            dataGridView1.Columns["DayShift"].HeaderText = "משמרת 8.5 שעות";
            dataGridView1.Columns["Weekend"].HeaderText = "משמרת 12 שעות";
            dataGridView1.Columns["LastModifier"].HeaderText = @"עודכן ע'י";
            dataGridView1.Columns["WDEPT"].HeaderText = "מחלקה";
            dataGridView1.Columns["AddedTime"].Visible = false;

            //dataGridView1.Columns["RMPROD"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            //dataGridView1.Columns["RMPROD"].Width = 150;
            //dataGridView1.Columns["One_Worker"].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
            //dataGridView1.Columns["One_Worker"].Width = 85;

            /// DGV_New
            DGV_New.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_New.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_New.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            /// DGV_Similar
            DGV_Similar.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Similar.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_Similar.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // DGV_Weightings
            DGV_Weightings.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Weightings.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_Weightings.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            // DGV_Gaps
            DGV_Gaps.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            DGV_Gaps.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            DGV_Gaps.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            Lbl_NormCount.Text = "סך התקנים: " + dataGridView1.Rows.Count.ToString();
            Txt_Cat.Visible = true;
            Txt_Mach.Visible = true;
            Txt_JumboMain.Visible = true;
            Txt_TiresInShfitMain.Visible = true;
        }

        /// <summary>
        /// סינון הרשומות על פי מק'ט מוזן
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Txt_Cat.Text))
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Empty;
            }
            else
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("RMPROD LIKE '{0}%'", Txt_Cat.Text);
            }
        }

        private void Histogram_GetToolTipText(object sender, ToolTipEventArgs e)
        {
            // Check selected chart element and set tooltip text for it
            switch (e.HitTestResult.ChartElementType)
            {
                case ChartElementType.DataPoint:
                    var dataPoint = e.HitTestResult.Series.Points[e.HitTestResult.PointIndex];
                    e.Text = string.Format("תקן (דק' נטו): {0}   \nכמות צמיגים: {1} ", dataPoint.XValue, dataPoint.YValues[0]);
                    break;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            PB_NewItems.Visible = true;
            Lbl_Prog.Visible = true;
            BackgroundWorker bgw = new BackgroundWorker();
            bgw.DoWork += new DoWorkEventHandler(bgw_GetCatWithoutNorm); // שליפת המקטים שנדרש לעדכן להם תקן

            bgw.RunWorkerAsync();
        }

        void bgw_GetCatWithoutNorm(object sender, DoWorkEventArgs e)
        {
            var backgroundWorker = sender as BackgroundWorker;
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker.WorkerReportsProgress = true;
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            DataTable DTNew = new DataTable();
            List<Item> NewItems = new List<Item>();
            List<Tuple<string, string>> groups = GetBuildingGroups();
            string StrSql = @"select distinct izprod,substring(BCLAC,1,1) as BCLAC, '' as Norm
                            from bpcsfali.shmal01
                            left join BPCSFV30.MBM on BPROD=IZPROD
                            WHERE  substring(BCLAC,1,1) IN ('M','N','L') --AND IZPROD='22706500AL    1'
                            --Fetch first 70 rows only ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            int prog = 1;
            foreach (DataRow row in DT.Rows)
            {
                Item cat = new Item(row["izprod"].ToString(), row["BCLAC"].ToString()); // מק"ט אב
                cat = cat.GetChildItem();
                while (cat.Bclac != null)
                {
                    if (cat.CatNumber != null)
                    {
                        cat.GetItemInfo();
                        DataRow[] dr = DTAllNorms.Select("RMPROD ='" + cat.CatNumber + "' AND MOFRE1 ='" + cat.Group + "' ");
                        if (dr.Length == 0)
                        {
                            if (cat.Mach == null)
                            {
                                break;
                            }
                            cat.Group = int.Parse(groups.FirstOrDefault(x => x.Item1 == cat.Mach).Item2);
                            NewItems.Add(cat);
                            DGV_New.Invoke((MethodInvoker)delegate () { DGV_New.DataSource = ""; });
                            DGV_New.Invoke((MethodInvoker)delegate () { DGV_New.DataSource = NewItems; });
                            DGV_New.Invoke((MethodInvoker)delegate () { DGV_New.Columns["BCLAC"].Visible = false; });
                            DGV_New.Invoke((MethodInvoker)delegate () { DGV_New.Columns["Norm"].DefaultCellStyle.BackColor = Color.PaleGreen; });
                        }
                    }
                    cat = cat.GetChildItem();
                    cat = new Item(cat.CatNumber, cat.Bclac); // מק"ט בן
                }
                Int32 percentage = (Int32)Math.Round((double)(prog * 100) / DT.Rows.Count);
                backgroundWorker.ReportProgress(percentage, null);
                prog++;
            }

            // עיצוב הגריד
            DGV_New.Invoke((MethodInvoker)delegate () { DGV_New.ReadOnly = false; });
            foreach (DataGridViewColumn c in DGV_New.Columns)
            {
                c.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                DGV_New.Invoke((MethodInvoker)delegate () { c.ReadOnly = true; });
                if (c.DataPropertyName == "Norm")
                {
                    DGV_New.Invoke((MethodInvoker)delegate () { c.ReadOnly = false; });
                }
                if (c.DataPropertyName == "CatNumber")
                {
                    DGV_New.Invoke((MethodInvoker)delegate () { c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft; });
                }
                if (c.DataPropertyName == "RMPROD")
                {
                    c.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
            if (DGV_New.Rows.Count > 0)
            {
                DGV_New.Columns["CatNumber"].HeaderText = "מק'ט";
                DGV_New.Columns["Mach"].HeaderText = "מכונה";
                DGV_New.Columns["WorkCenter"].HeaderText = "מרכז עבודה";
                DGV_New.Columns["Group"].HeaderText = "קבוצה";
                DGV_New.Columns["Norm"].HeaderText = "תקן - צמיגים למשמרת";
            }


            // בדיקה האם קיימים ג'מבו או טרקטורים ואם כן להציג את הנתונים הנוספים הרלוונטיים
            foreach (DataGridViewRow row in DGV_New.Rows)
            {
                if (row.Cells["Mach"].Value.ToString() == "12" || row.Cells["Mach"].Value.ToString() == "16" || row.Cells["Mach"].Value.ToString() == "73" || row.Cells["Mach"].Value.ToString() == "75")
                {
                    foreach (DataGridViewCell c in row.Cells)
                    {
                        c.Style.BackColor = Color.Red;
                    }
                }
            }
        }


        private void backgroundWorker1_ProgressChanged(object sender,
    ProgressChangedEventArgs e)
        {
            // Change the value of the ProgressBar to the BackgroundWorker progress.
            PB_NewItems.Value = e.ProgressPercentage;
            // Set the text.
            //PB_NewItems.Text = e.ProgressPercentage.ToString();
            //     PB_NewItems.CreateGraphics().DrawString(PB_NewItems.Value.ToString() + "%", new Font("Arial",
            //(float)10.25, FontStyle.Bold),
            //Brushes.Black, new PointF(215 / 2 - 10, 23 / 2 - 7));
            if (PB_NewItems.Value == 100)
            {
                PB_NewItems.Visible = false;
                Lbl_Prog.Visible = false;
            }
        }

        #region
        //    private void GetNewCat(DataTable DT, List<Item> NewItems)
        //{
        //    List<Tuple<string, string>> groups = GetBuildingGroups();
        //    //List<Item> NewItems = new List<Item>();
        //    foreach (DataRow row in DT.Rows)
        //    {
        //        Item cat = new Item(row["izprod"].ToString(), row["BCLAC"].ToString()); // מק"ט אב
        //        cat = cat.GetChildItem();
        //        while (cat.Bclac != "L" && cat.Bclac != null)
        //        {
        //            if (cat.CatNumber != null)
        //            {
        //                cat.GetItemInfo();
        //                DataRow[] dr = DTAllNorms.Select("RMPROD ='" + cat.CatNumber + "' AND MAC ='" + cat.Mach + "' ");
        //                if (dr.Length == 0)
        //                {
        //                    cat.Group = int.Parse(groups.FirstOrDefault(x => x.Item1 == cat.Mach).Item2);
        //                    NewItems.Add(cat);
        //                    //DGV_New.Rows.Add(cat.CatNumber, cat.Mach, cat.Group);
        //                    //DGV_New.Refresh();
        //                }
        //            }
        //            cat = new Item(cat.GetChildItem().CatNumber); // מק"ט בן
        //        }
        //    }
        //}
        #endregion
        /// <summary>
        /// Gets the building groups list 
        /// </summary>
        /// <returns>List of Machine and it's group </returns>
        public List<Tuple<string, string>> GetBuildingGroups()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            List<Tuple<string, string>> groups = new List<Tuple<string, string>>();
            string StrSql = @"select distinct MOMACH,MOFRE1
                            FROM BPCSFALI.LMHO
                            WHERE MOWRKC between 750000 and 760000 and MOFRE1 <>'' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            foreach (DataRow row in DT.Rows)
            {
                groups.Add(new Tuple<string, string>(row["MOMACH"].ToString(), row["MOFRE1"].ToString()));
            }
            return groups;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "RMPROD") // ציור היסטוגרמה בלחיצה כפולה על מספר המקט
            {
                DbServiceSQL dbs = new DbServiceSQL();
                DataTable DT = new DataTable();
                DataTable DT1 = new DataTable();
                // משתני נתונים
                int F = dataGridView1.CurrentCell.RowIndex;
                string catNum = dataGridView1.Rows[F].Cells["RMPROD"].Value.ToString();
                int grp = int.Parse(dataGridView1.Rows[F].Cells["MOFRE1"].Value.ToString());
                double tiresCount = Math.Round(double.Parse(dataGridView1.Rows[F].Cells["num"].Value.ToString()), 0);
                decimal norm = 0;

                // נתוני ציר אופקי 90 מעלות עומד לא שוכב
                Histogram.ChartAreas["ChartArea1"].AxisX.IsLabelAutoFit = true;
                Histogram.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = 0;

                // קביע גודל פונט לגרף לפי צירים
                Histogram.ChartAreas["ChartArea1"].AxisX.TitleFont = new Font("Arial", 15, FontStyle.Bold);
                Histogram.ChartAreas["ChartArea1"].AxisY.TitleFont = new Font("Arial", 15, FontStyle.Bold);

                // Reset Chart Data
                Histogram.Series["Tires"].Points.Clear();
                Histogram.Series["Norm"].Points.Clear();
                Histogram.Titles.Clear();

                //Set the titles of the X and Y axes
                Histogram.ChartAreas["ChartArea1"].AxisX.Title = "Time";
                Histogram.ChartAreas["ChartArea1"].AxisY.Title = "Tires";
                //Set the Intervals of the X and Y axes, 
                //Histogram.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                Histogram.ChartAreas["ChartArea1"].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;

                Histogram.ChartAreas["ChartArea1"].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep30;

                // Grid lines style
                Histogram.ChartAreas["ChartArea1"].AxisY.MajorGrid.LineColor = Color.LightGray;
                Histogram.ChartAreas["ChartArea1"].AxisX.MajorGrid.LineColor = Color.LightGray;
                Histogram.ChartAreas["ChartArea1"].AxisX.ScaleView.Zoomable = true;
                this.Histogram.GetToolTipText += this.Histogram_GetToolTipText;

                Histogram.Series["Tires"].MarkerStyle = MarkerStyle.Circle;
                Histogram.Series["Norm"].MarkerStyle = MarkerStyle.Circle;

                // SQL Server התחברות ל
                string StrSql = "";
                StrSql = @"Select top 30 Time_Diff, COUNT(time_Diff) as count,PERCENTILE_CONT(0.3) WITHIN GROUP (ORDER BY Time_Diff ) OVER (PARTITION BY [Cat_Number],[Mac_Name]) as Norm,  rank() over (order by count(*) desc) as rank1
                           from [Production].[dbo].[Building_Norm]
                           WHERE Cat_Number=? and [group]=?
					       group by Time_Diff,[Cat_Number],[Mac_Name]";
                SqlParameter cat = new SqlParameter("@cat", catNum);
                SqlParameter group = new SqlParameter("@grp", grp);
                DT = dbs.GetDataSetByQuery(StrSql, CommandType.Text, cat, group).Tables[0];

                int max = 0;
                foreach (DataRow row in DT.Rows)
                {
                    Histogram.Series["Tires"].Points.AddXY(decimal.Parse(row["Time_Diff"].ToString()), decimal.Parse(row["count"].ToString()));
                    max = int.Parse(row["count"].ToString()) > max ? int.Parse(row["count"].ToString()) : max;
                }

                StrSql = @"SELECT DISTINCT Cat_Number, [group] AS GroupName,
                             PERCENTILE_CONT(0.3) WITHIN GROUP(ORDER BY Time_Diff) OVER(PARTITION BY [Cat_Number],
                             [group]) AS Norm
                             FROM[Production].[dbo].[Building_Norm]
                             WHERE Cat_Number = ? and [Group] = ?  ";
                DT = dbs.GetDataSetByQuery(StrSql, CommandType.Text, cat, group).Tables[0];
                if (DT.Rows.Count > 0)
                {
                    norm = decimal.Parse(DT.Rows[0]["Norm"].ToString());
                    Histogram.Series["Norm"].Points.AddXY(norm, decimal.Parse(max.ToString()));
                    // כותרת הגרף
                    Title T = new Title(dataGridView1.Rows[F].Cells["RMPROD"].Value.ToString() + " - " + grp);
                    T.Font = new System.Drawing.Font("Tahome", 16, FontStyle.Bold);
                    T.ForeColor = Color.Blue;
                    Histogram.Titles.Add(T);

                    Title T1 = new Title(string.Format(" זמן תקן (דק' נטו): {0} דק, מבוסס על {1} שקילות", norm, tiresCount) + Environment.NewLine);
                    T1.Font = new System.Drawing.Font("Tahome", 14, FontStyle.Bold);
                    T1.ForeColor = Color.Crimson;
                    Histogram.Titles.Add(T1);

                    // שליפת כמות  צמיגים מתחת לתקן
                    StrSql = @"SELECT count(*) as Below
                               FROM[Production].[dbo].[Building_Norm]
	                           WHERE Cat_Number = ? and [Group] = ? and Time_Diff < ?";
                    SqlParameter n = new SqlParameter("@norm", norm);
                    DT1 = dbs.GetDataSetByQuery(StrSql, CommandType.Text, cat, group, n).Tables[0];

                    Title T2 = new Title(string.Format("ב {0} צמיגים שנשקלו זמן הבנייה היה פחות מ: {1} דקות ", DT1.Rows[0]["Below"], norm));
                    T2.Font = new System.Drawing.Font("Tahome", 12, FontStyle.Bold);
                    T2.ForeColor = Color.DimGray;
                    Histogram.Titles.Add(T2);

                    Title T3 = new Title(string.Format("כמות צמיגים למשמרת של 8.5 שעות: {0} ", Math.Floor(365 / norm)));
                    T3.Font = new System.Drawing.Font("Tahome", 12, FontStyle.Bold);
                    T3.ForeColor = Color.DimGray;
                    Histogram.Titles.Add(T3);
                    // עיצוב כותרות הגרף
                    Histogram.Titles["Title3"].Alignment = ContentAlignment.MiddleRight;
                    Histogram.Titles["Title4"].Alignment = ContentAlignment.MiddleRight;
                    Histogram.ChartAreas[0].Position.Y = 100;
                    Histogram.ChartAreas[0].Position.Height = 60;
                    Histogram.ChartAreas[0].Position.Width = 95;
                    Histogram.Legends[0].Position.X = 0;
                    // קביעת מיקום מקרא            
                    Histogram.Legends[0].Docking = Docking.Left;
                }

                // set min & max of axis Y
                Histogram.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "#####";
                Histogram.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "#####";
                Histogram.ChartAreas["ChartArea1"].AxisX.LabelAutoFitStyle = LabelAutoFitStyles.LabelsAngleStep45;
                Histogram.ChartAreas["ChartArea1"].AxisY.Minimum = 0;
                Histogram.ChartAreas["ChartArea1"].AxisY.Maximum = max * 1.15;
            }
            else if (dataGridView1.Columns[e.ColumnIndex].Name == "RMOPR") // בלחיצה כפולה על מספר עובדים נפתח חלון עדכון מספר עובדים
            {
                int F = dataGridView1.CurrentCell.RowIndex;
                string mach = dataGridView1.Rows[F].Cells["mac"].Value.ToString();
                double operators = double.Parse(dataGridView1.Rows[F].Cells["RMOPR"].Value.ToString());

                using (NumOperators numOfOp = new NumOperators())
                {
                    numOfOp.Mach = mach;
                    numOfOp.NumOfOperators = operators;
                    if (numOfOp.ShowDialog() == DialogResult.OK)
                    {
                        //5new Task(() => LoadData()); // רפרוש הטבלה
                        LoadData();
                        foreach (DataGridViewRow row in dataGridView1.Rows)
                        {
                            if (row.Cells["mac"].Value.ToString() == mach && row.Cells["RMOPR"].Value.ToString() != numOfOp.NumOfOperators.ToString())
                            {
                                row.Cells["RMOPR"].Style.BackColor = Color.PaleVioletRed;
                            }
                        }
                    }
                }
            }
        }

        private void manageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageLogin f = new ManageLogin();
            f.ShowDialog();
        }
        // שליפת מק"טים בני דודים מקובץ התקנים
        private void DGV_New_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (DGV_New.Columns[e.ColumnIndex].Name == "CatNumber")
            {
                DGV_Similar.DataSource = "";
                DBService dbs = new DBService();
                DataTable DT = new DataTable();
                int F = DGV_New.CurrentCell.RowIndex;
                string catNum = DGV_New.Rows[F].Cells["CatNumber"].Value.ToString();
                string grp = DGV_New.Rows[F].Cells["Group"].Value.ToString();
                string StrSql = @"SELECT RMPROD,RMWRKC,RMMACI,MOFRE1,CAST(FLOOR(365 / (1440/RMTKN*0.72) * " + Factor + @") as decimal(10,2)) as RMTKN
                                  FROM BPCSFALI.FRTML01 
                                  left join BPCSFALI.LMHOL01 ON MOMACH=RMMACI and  MOWRKC=RMWRKC
                                  WHERE SUBSTRING(RMPROD,1,10)=substring('" + catNum + "',1,10) AND RMWRKC BETWEEN 750000 AND 760000 AND MOFRE1 = '" + grp + "' ";
                //,Case
                //           WHEN RMMACI = '12' OR RMMACI = '16' OR RMMACI = '73' OR RMMACI = '75' THEN CASE WHEN substring(RMPROD,10,1)= '0' THEN 2.5 * CAST(FLOOR(1440 / RMTKN * 0.72 * " + Factor + @") as decimal(10, 2)) ELSE 1.666 * CAST(FLOOR(1440 / RMTKN * 0.72 * " + Factor + @") as decimal(10, 2)) END ELSE  CAST(FLOOR(1440 / RMTKN * 0.72 * " + Factor + @") as decimal(10, 2))
                //                END as Jumbo, 0 as TiresInShift
                DT = dbs.executeSelectQueryNoParam(StrSql);
                DGV_Similar.DataSource = DT;
                DGV_Similar.Columns["RMPROD"].HeaderText = "מק'ט";
                DGV_Similar.Columns["RMWRKC"].HeaderText = "מרכז עבודה";
                DGV_Similar.Columns["MOFRE1"].HeaderText = "קבוצה";
                DGV_Similar.Columns["RMMACI"].HeaderText = "מכונה";
                DGV_Similar.Columns["RMTKN"].HeaderText = "תקן - צמיגים למשמרת";
                //DGV_Similar.Columns["Jumbo"].HeaderText = "זמן נטו לצמיג ירוק";
                //DGV_Similar.Columns["TiresInShift"].HeaderText = "צמיגים למשמרת (טרקטור / ג'מבו)";

                foreach (DataGridViewColumn column in DGV_Similar.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.Automatic;
                    column.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    if (column.DataPropertyName == "RMPROD")
                    {
                        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    }
                }

                //// הצגת שקילות מק"ט ובני דודים
                DbServiceSQL dbSql = new DbServiceSQL();
                DataTable DTJ = new DataTable();
                StrSql = @"  select distinct Cat_Number,Mac_Name,[Group],cast(365 / (PERCENTILE_CONT(0.3) WITHIN GROUP (ORDER BY Time_Diff ) OVER (PARTITION BY [Cat_Number],Mac_Name)) * 0.72 as decimal (3,0)) as Norm
			                               from[Production].[dbo].[Building_Norm]
                                           where cat_number like ? and [Group] = ?
										   group by  Cat_Number,Mac_Name,Time_Diff,[Group]
                                           order by Cat_Number,Mac_Name,Norm desc";
                SqlParameter cat = new SqlParameter("@cat", catNum.Substring(0, 10) + "%");
                SqlParameter group = new SqlParameter("@grp", grp);
                DTJ = dbSql.GetDataSetByQuery(StrSql, CommandType.Text, cat, group).Tables[0];
                DGV_Weightings.DataSource = DTJ;

                // עיצוב הגריד
                DGV_Weightings.Columns["Cat_Number"].HeaderText = "מק'ט";
                DGV_Weightings.Columns["Mac_Name"].HeaderText = "מכונה";
                DGV_Weightings.Columns["Group"].HeaderText = "קבוצה";
                DGV_Weightings.Columns["Norm"].HeaderText = "תקן - צמיגים למשמרת";

                foreach (DataGridViewColumn column in DGV_Weightings.Columns)
                {
                    column.SortMode = DataGridViewColumnSortMode.Automatic;
                    column.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                    column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    if (column.DataPropertyName == "Cat_Number")
                    {
                        column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    }
                    if (column.DataPropertyName == "JUMBO" || column.DataPropertyName == "TIRESINSHIFT")
                    {
                        column.DefaultCellStyle.BackColor = Color.NavajoWhite;
                    }
                }

                DGV_Weightings.Columns.Add("Jumbo", "זמן נטו לצמיג ירוק");
                DGV_Weightings.Columns.Add("TiresInShift", "צמיגים למשמרת (טרקטור / ג'מבו)");

                foreach (DataGridViewRow row in DGV_Weightings.Rows)
                {
                    if (row.Cells["Group"].Value.ToString() == "12" || row.Cells["Group"].Value.ToString() == "16" || row.Cells["Group"].Value.ToString() == "73" || row.Cells["Group"].Value.ToString() == "75")
                    {
                        if (row.Cells["Cat_Number"].Value.ToString().Substring(9,1) == "0")
                        {
                            row.Cells["Jumbo"].Value = Math.Round(2.5 * double.Parse(row.Cells["Norm"].Value.ToString()));
                        }
                        else
                        {
                            row.Cells["Jumbo"].Value = Math.Round(1.6 * double.Parse(row.Cells["Norm"].Value.ToString()));
                        }
                        row.Cells["TiresInShift"].Value = Math.Round(365 / double.Parse(row.Cells["JUMBO"].Value.ToString()));  // הזנת מס צמיגים ירוקים למשמרת
                    }
                }
                
            }
        }

        public void Btn_Gaps_Click(object sender, EventArgs e)
        {
            if (TgetNorms.IsCompleted) // בדיקה האם קריאת הנתונים הסתיימה כדי לוודא שיש נתונים
            {
                DataTable DT = new DataTable();
                DataColumn co = new DataColumn("מק'ט", typeof(string));
                DT.Columns.Add(co);
                co = new DataColumn("מכונה", typeof(string));
                DT.Columns.Add(co);
                co = new DataColumn("קבוצה", typeof(string));
                DT.Columns.Add(co);
                co = new DataColumn("('תקן - זמן נטו לצמיג (דק ", typeof(double));
                DT.Columns.Add(co);
                co = new DataColumn("זמן מחושב שקילות + מגדלים", typeof(double));
                DT.Columns.Add(co);
                co = new DataColumn("זמן נטו לצמיג ירוק", typeof(double));
                DT.Columns.Add(co);
                co = new DataColumn("מס צמיגים במשמרת", typeof(double));
                DT.Columns.Add(co);

                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = null;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    var X = DTWeightingNorms.Select("Cat_number ='" + row.Cells["RMPROD"].Value + "' AND GroupName = '" + row.Cells["MOFRE1"].Value + "' ");
               
                    if (X.Length > 0)
                    {
                        if (row.Cells["RMPROD"].Value.ToString().Substring(9,1) == "0") // האם צמיג ירוק
                        {
                            if (double.Parse(row.Cells["Jumbo"].Value.ToString()) / (double.Parse(X[0]["Norm"].ToString()) + double.Parse(row.Cells["ADDEDTIME"].Value.ToString())) > 1 + double.Parse(Txt_Percentage.Text) / 100)
                            {
                                DT.Rows.Add(row.Cells["RMPROD"].Value, row.Cells["mac"].Value, row.Cells["MOFRE1"].Value, row.Cells["RMTKN"].Value, double.Parse(X[0]["Norm"].ToString()) + double.Parse(row.Cells["ADDEDTIME"].Value.ToString()), row.Cells["Jumbo"].Value, row.Cells["TiresInShift"].Value);
                            }
                        }
                        else  // במידה ו קרקס
                        {
                            if (double.Parse(row.Cells["RMTKN"].Value.ToString()) / (double.Parse(X[0]["Norm"].ToString()) + double.Parse(row.Cells["ADDEDTIME"].Value.ToString())) > 1 + double.Parse(Txt_Percentage.Text) / 100)
                            {
                                DT.Rows.Add(row.Cells["RMPROD"].Value, row.Cells["mac"].Value, row.Cells["MOFRE1"].Value, row.Cells["RMTKN"].Value, double.Parse(X[0]["Norm"].ToString()) + double.Parse(row.Cells["ADDEDTIME"].Value.ToString()), row.Cells["Jumbo"].Value, row.Cells["TiresInShift"].Value);
                            }
                        }
                        
                    }
                }

                DGV_Gaps.DataSource = DT;
                // אפשור עדכון תקנים 
                DGV_Gaps.ReadOnly = false;
                foreach (DataGridViewColumn c in DGV_Gaps.Columns)
                {
                    if (c.DataPropertyName == "מק'ט")
                    {
                        c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    }
                    c.DefaultCellStyle.Font = new Font("Tahoma", 15F, GraphicsUnit.Pixel);
                    c.ReadOnly = true;
                    c.DisplayIndex = c.Index;

                    if (c.DataPropertyName == "('תקן - זמן נטו לצמיג (דק ")
                    {
                        c.ReadOnly = false;  // editable
                        c.DefaultCellStyle.BackColor = Color.PaleGreen;
                    }

                    if (c.DataPropertyName == "מס צמיגים במשמרת")
                    {
                        c.DefaultCellStyle.BackColor = Color.LightSkyBlue;
                    }

                    if (c.DataPropertyName == "זמן נטו לצמיג ירוק")
                    {
                        c.DefaultCellStyle.BackColor = Color.NavajoWhite;
                    }

                }
                Lbl_CountGaps.Text = "סך הפערים: " + DGV_Gaps.Rows.Count.ToString();
                Txt_JumboNorm.Visible = true;
                Txt_TiresInShift.Visible = true;
            }
            else
            {
                MessageBox.Show("אנא נסה שנית מאוחר יותר");
            }
        }

        private void Btn_NewNorm_Click(object sender, EventArgs e)
        {
            DialogResult D = MessageBox.Show("?אתה בטוח שאתה רוצה לעדכן את תקנים", "עדכון תקנים", MessageBoxButtons.YesNo);
            if (D == DialogResult.Yes)
            {
                foreach (DataGridViewRow row in DGV_New.Rows)
                {
                    Item I = new Item(row.Cells["CatNumber"].Value.ToString());
                    I.Norm = double.Parse(row.Cells["Norm"].Value.ToString());
                    I.Group = int.Parse(row.Cells["Group"].Value.ToString());
                    I.Mach = row.Cells["Mach"].Value.ToString();
                    I.AddNewNorm();
                }
                MessageBox.Show("התקנים עודכנו בהצלחה");
            }
        }
        // שמירת השורות שהתעדכנו כדי לעדכן בהמשך
        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            bool b = rowsToUpd.ContainsKey(dataGridView1.CurrentRow.Index);
            if (!b)
                rowsToUpd.Add(dataGridView1.CurrentRow.Index, dataGridView1.CurrentRow.Index);
        }
        // עדכון התקנים החדשים - ריצה רק על השורות שעודכנו בגריד
        private void Btn_UpdateNorm_Click(object sender, EventArgs e)
        {
            DialogResult D = MessageBox.Show("?אתה בטוח שאתה רוצה לעדכן את תקנים", "עדכון תקנים", MessageBoxButtons.YesNo);
            if (D == DialogResult.Yes)
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = null;
                foreach (var norm in rowsToUpd)
                {
                    Item I = new Item(dataGridView1.Rows[norm.Value].Cells["RMPROD"].Value.ToString());
                    I.Norm = 1440 / double.Parse(dataGridView1.Rows[norm.Value].Cells["RMTKN"].Value.ToString()) * 0.72;
                    I.Group = int.Parse(dataGridView1.Rows[norm.Value].Cells["MOFRE1"].Value.ToString());
                    I.UpdateNorm();
                }
                MessageBox.Show("התקנים עודכנו בהצלחה");
            }
        }

        private void Txt_Percentage_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
     (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            DialogResult D = MessageBox.Show("?אתה בטוח שאתה רוצה לעדכן את תקנים", "עדכון תקנים", MessageBoxButtons.YesNo);
            if (D == DialogResult.Yes)
            {
                foreach (DataGridViewRow norm in DGV_Gaps.Rows)
                {
                    Item I = new Item(norm.Cells["RMPROD"].Value.ToString());
                    I.Norm = double.Parse(norm.Cells["RMTKN"].Value.ToString());
                    I.Group = int.Parse(norm.Cells["MOFRE1"].Value.ToString());
                    I.UpdateNorm();
                }
                MessageBox.Show("התקנים עודכנו בהצלחה");
            }
        }

        private void DGV_Gaps_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            bool b = rowsToUpd.ContainsKey(DGV_Gaps.CurrentRow.Index);
            if (b)
                rowsToUpd.Add(DGV_Gaps.CurrentRow.Index, DGV_Gaps.CurrentRow.Index);
        }

        private void DGV_New_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            Txt_CatNum.Text = DGV_New.Rows[DGV_New.CurrentCell.RowIndex].Cells["CatNumber"].Value.ToString();
            if (Txt_CatNum.Text.Substring(9, 1) == "0")
                Lbl_Tires.Text = @"מספר צמיגים ירוקים מומלצים למשמרת";
            else
                Lbl_Tires.Text = @"מספר קרקסים מומלצים למשמרת";

            Item I = new Item(DGV_New.CurrentRow.Cells["CatNumber"].Value.ToString(), DGV_New.CurrentRow.Cells["Mach"].Value.ToString(), double.Parse(DGV_New.CurrentRow.Cells["Norm"].Value.ToString()));
            if (I.CheckIfJumbo())
            {
                Grp_Jumbo.Visible = true;
            }
            else
            {
                Grp_Jumbo.Visible = false;
            }
        }

        private void Txt_Green_TextChanged(object sender, EventArgs e)
        {
            if (Txt_Green.Text.Length > 0)
            {
                if (Txt_CatNum.Text.Substring(10, 1) == "0")
                {
                    Txt_Calc.Text = (double.Parse(Txt_Green.Text) / level0).ToString();
                }
                else
                {
                    Txt_Calc.Text = (double.Parse(Txt_Green.Text) / level1).ToString();
                }
            }
            else
            {
                Txt_Calc.Text = "";
                Lbl_Tires.Text = "";
            }
        }

        private void Txt_Green_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void Txt_Calc_TextChanged(object sender, EventArgs e)
        {
            if (Txt_Calc.Text.Length > 0)
            {
                Txt_Minutes.Text = decimal.Parse(((365 / (double.Parse(Txt_Calc.Text)) * 10 + 0.5) / 10).ToString()).ToString("F");
            }
            else
            {
                Txt_Minutes.Text = "";
            }
        }

        private void DGV_New_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (DGV_New.Columns[e.ColumnIndex].Name == "Norm") // 1 should be your column index
            {
                int i;

                if (!int.TryParse(Convert.ToString(e.FormattedValue), out i))
                {
                    e.Cancel = true;
                }
                else
                {
                    // the input is numeric 
                }
            }
        }

        private void Txt_Mach_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Txt_Mach.Text))
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Empty;
            }
            else
            {
                (dataGridView1.DataSource as DataTable).DefaultView.RowFilter = string.Format("mac LIKE '{0}%'", Txt_Mach.Text);
            }
        }

        private void Btn_UpdateFactor_Click(object sender, EventArgs e)
        {
            Form1.UpdateFactor(double.Parse(Txt_Factor.Text));
            MessageBox.Show("הפקטור שונה בהצלחה");
        }

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows[e.RowIndex].Cells["RMTKN"].Value != System.DBNull.Value)
            {
                Item I = new Item(dataGridView1.Rows[e.RowIndex].Cells["RMPROD"].Value.ToString());
                I.Norm = (1440 / double.Parse(dataGridView1.Rows[e.RowIndex].Cells["RMTKN"].Value.ToString()) * 0.72);
                I.Group = int.Parse(dataGridView1.Rows[e.RowIndex].Cells["MOFRE1"].Value.ToString());
                I.UpdateNorm();
                new Task(() => LoadData()); // רפרוש הטבלה
            }
            else
            {
                dataGridView1.CurrentCell = dataGridView1.Rows[e.RowIndex].Cells["RMTKN"];
                MessageBox.Show("!תקן לא יכול להיות ריק");
            }
        }

        private void DGV_Gaps_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (DGV_Gaps.Rows[e.RowIndex].Cells["('תקן - זמן נטו לצמיג (דק "].Value != System.DBNull.Value)
            {
                Item I = new Item(DGV_Gaps.Rows[e.RowIndex].Cells["מק'ט"].Value.ToString());
                I.Norm = (1440 / double.Parse(DGV_Gaps.Rows[e.RowIndex].Cells["('תקן - זמן נטו לצמיג (דק "].Value.ToString()) * 0.72);
                I.Group = int.Parse(DGV_Gaps.Rows[e.RowIndex].Cells["קבוצה"].Value.ToString());
                I.UpdateNorm();

                DGV_Gaps.CurrentCell.Style.BackColor = Color.White;
                new Task(() => LoadData()); // רפרוש הטבלה הראשית כדי לשמור על נתונים עדכניים
            }
            else
            {
                DGV_Gaps.CurrentCell = DGV_Gaps.Rows[e.RowIndex].Cells["('תקן - זמן נטו לצמיג (דק "];
                MessageBox.Show("!תקן לא יכול להיות ריק");
            }
        }

        private void DGV_New_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Item I = new Item(DGV_New.Rows[e.RowIndex].Cells["CatNumber"].Value.ToString());
            I.Norm = double.Parse(DGV_New.Rows[e.RowIndex].Cells["Norm"].Value.ToString());
            I.Group = int.Parse(DGV_New.Rows[e.RowIndex].Cells["Group"].Value.ToString());
            I.Mach = DGV_New.Rows[e.RowIndex].Cells["Mach"].Value.ToString();
            I.AddNewNorm();

            DGV_New.CurrentCell.Style.BackColor = Color.White;
            new Task(() => LoadData()); // רפרוש הטבלה הראשית כדי לשמור על נתונים עדכניים
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "RMTKN") // 1 should be your column index
            {
                int i;

                if (!int.TryParse(Convert.ToString(e.FormattedValue), out i))
                {
                    e.Cancel = true;
                }
                else
                {
                    // the input is numeric 
                }
            }
        }

        private void DGV_Gaps_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (DGV_Gaps.Columns[e.ColumnIndex].Name == "('תקן - זמן נטו לצמיג (דק ") // 1 should be your column index
            {
                int i;

                if (!int.TryParse(Convert.ToString(e.FormattedValue), out i))
                {
                    e.Cancel = true;
                }
                else
                {
                    // the input is numeric 
                }
            }
        }
    }
}
