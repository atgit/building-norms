﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuildingNorms
{
    public class Item
    {
        string catNumber;
        string bclac;
        string mach;
        int workcenter;
        int group;
        double norm;

        public Item()
        {

        }
        public Item(string catNumber)
        {
            CatNumber = catNumber;
        }
        public Item(string catNumber, string bclac)
        {
            this.CatNumber = catNumber;
            this.Bclac = bclac;
        }

        public Item(string catNumber, string bclac, string mach)
        {
            this.CatNumber = catNumber;
            this.Bclac = bclac;
            this.Mach = mach;
        }
        public Item(string catNumber, string mach, double norm)
        {
            CatNumber = catNumber;
            Mach = mach;
            Norm = norm;
        }
        public Item(string catNumber, string mach, int group)
        {
            CatNumber = catNumber;
            Mach = mach;
            Group = group;
        }

        public string CatNumber { get => catNumber; set => catNumber = value; }
        public string Bclac { get => bclac; set => bclac = value; }
        public string Mach { get => mach; set => mach = value; }
        public int Workcenter { get => workcenter; set => workcenter = value; }
        public int Group { get => group; set => group = value; }
        public double Norm { get => norm; set => norm = value; }


        /// <summary>
        /// Function returns an Item object which is the child object of the item that called the function
        /// Mainly will be used recursively 
        /// </summary>
        /// <returns> Item</returns>
        public Item GetChildItem()
        {
            DataTable DT = new DataTable();
            DBService dbs = new DBService();
            string StrSql = @"SELECT BPROD,BCHLD,substring(BCLAC,1,1) as BCLAC
                           FROM BPCSFV30.MBM 
                           WHERE BPROD ='" + CatNumber + "' AND substring(BCLAC,1,1) IN ('M','N','L') ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            Item I = new Item();
            if (DT.Rows.Count > 0)
            {
                I.CatNumber = DT.Rows[0]["BCHLD"].ToString();
                I.Bclac = DT.Rows[0]["BCLAC"].ToString();
            }
            return I;
        }

        /// <summary>
        /// Sets the item's WorkCenter, Machine and Group
        /// </summary>
        public void GetItemInfo()
        {
            DataTable DT = new DataTable();
            DBService dbs = new DBService();
            //string StrSql = @"SELECT RWRKC,MHMACH,RMOPR
            //               FROM BPCSFV30.FRT 
            //               LEFT JOIN BPCSFV30.LMH ON MHWRKC=RWRKC
            //               LEFT JOIN BPCSFALI.FRTML01 ON RMPROD = RPROD AND RMMACI = MHMACH
            //               WHERE RPROD ='" + CatNumber + "' AND MHWRKC BETWEEN 750000 AND 760000 AND MOFRE1 <> 0";
            string StrSql = @"SELECT RWRKC,MHMACH,MOFRE1
                           FROM BPCSFV30.FRT 
                           LEFT JOIN BPCSFV30.LMH ON MHWRKC=RWRKC
                           LEFT JOIN BPCSFALI.FRTML01 ON RMPROD = RPROD AND RMMACI = MHMACH
                           left join BPCSFALI.LMHOL01 ON MOMACH=MHMACH and  MOWRKC=RWRKC
                           WHERE RPROD ='" + CatNumber + "' AND MHWRKC  BETWEEN 750000 AND 760000 AND MOFRE1 <> ''";


            // הוספת סינון גם מרכזי עבודה
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                this.Workcenter = int.Parse(DT.Rows[0]["RWRKC"].ToString());
                this.Mach = DT.Rows[0]["MHMACH"].ToString();
                this.Group = int.Parse(DT.Rows[0]["MOFRE1"].ToString());
            }
            else
            {
                MessageBox.Show(string.Format("לפריט {0} לא מקושר מרכז עבודה מתאים. אנא טיפולכם בהקדם" ,  catNumber));
            }
        }

        /// <summary>
        /// Add a new Norm
        /// </summary>
        public void AddNewNorm()
        {
            if (Norm > 0)
            {
                DataTable DT = new DataTable();
                DBService dbs = new DBService();
                double TiresPerDay = 0;
                #region
                //if (CheckIfJumbo())
                //{
                //    if (CatNumber.Substring(10,1) == "0") // האם ירוק
                //    {
                //        TiresPerDay = Math.Round(1440 / Norm * 0.72, 2) * Form2.level0; // המרה למספר צמיגים ביום;
                //    }
                //    else  // האם קרקס
                //    {
                //        TiresPerDay = Math.Round(1440 / Norm * 0.72, 2) * Form2.level1; // המרה למספר צמיגים ביום;
                //    }
                //}
                //else
                //{
                //    TiresPerDay = Math.Round(1440 / Norm * 0.72, 2); // המרה למספר צמיגים ביום
                //}
                #endregion
                TiresPerDay = Math.Round(1440 / Norm * 0.72, 2); // המרה למספר צמיגים ביום
                string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userName = userName.Substring(userName.IndexOf("\\") + 1, (userName.Length - (userName.IndexOf("\\") + 1)));
                int NumOperators = GetNumOfOperators();
                /// לבחור כמות עובדים מהטבלה של צביקה
                string StrSql = @"INSERT INTO BPCSFALI.FRTM
                              values('MT','F1','" + CatNumber + "'," + Workcenter + ",'" + Mach + "',''," + NumOperators + ",0,0,0,0," + TiresPerDay + ",0,0,0,0,0,0,0,'','','','','" + userName + "'," + DateTime.Now.ToString("yyyyMMdd") + ",'" + userName + "')";
                dbs.executeInsertQuery(StrSql);
            }
        }
        /// <summary>
        /// עדכון תקן קיים בקובץ התקנים.
        /// העדכון מתבצע לכל המכונות בקבוצת המכונות של המק"ט הנבחר שמופיעות במרכז העבודה של הפריט
        /// </summary>
        public void UpdateNorm()
        {
            if (Norm > 0)
            {
                DBService dbs = new DBService();
                DataTable DT = new DataTable();
                string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                userName = userName.Substring(userName.IndexOf("\\") + 1, (userName.Length - (userName.IndexOf("\\") + 1)));

                string StrSql = @"UPDATE BPCSFALI.FRTML01                                             
                            SET RMTKN = " + Norm + ", RMJOB='" + userName + "' " +
                               " WHERE  RMPROD='" + CatNumber + "' " +
                               "and RMWRKC BETWEEN 750000 and 760000  " +
                               "and RMMACI in(SELECT RMMACI  " +
                               "FROM BPCSFALI.FRTML01  " +
                               "left join BPCSFV30.iiml01 ON RMPROD=IPROD " +
                               "left join BPCSFALI.LMHOL01 ON MOMACH=RMMACI and  MOWRKC=RMWRKC  " +
                               "WHERE RMPROD='" + CatNumber + "' and MOFRE1 = '" + Group + "') ";
                dbs.executeInsertQuery(StrSql);
            }
        }

        public bool CheckIfJumbo()
        {
            if (Mach == "12" || Mach == "16" || Mach == "73" || Mach == "75")
                return true;
            else
                return false;
        }

        public int GetNumOfOperators()
        {
            DBService dbs = new DBService();
            DataTable DT = new DataTable();
            string StrSql = @"SELECT RMOPR 
                              FROM BPCSFALI.FRTML01
                              WHERE RMMACI = '" + mach + "' FETCH FIRST ROW ONLY ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            return int.Parse(DT.Rows[0]["RMOPR"].ToString());
        }
    }
}
