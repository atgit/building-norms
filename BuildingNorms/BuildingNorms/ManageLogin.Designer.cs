﻿namespace BuildingNorms
{
    partial class ManageLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Login = new System.Windows.Forms.Button();
            this.Lbl_User = new System.Windows.Forms.Label();
            this.Lbl_Pass = new System.Windows.Forms.Label();
            this.Txt_User = new System.Windows.Forms.TextBox();
            this.Txt_Pass = new System.Windows.Forms.TextBox();
            this.Btn_Canacel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Btn_Login
            // 
            this.Btn_Login.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Login.Location = new System.Drawing.Point(86, 242);
            this.Btn_Login.Name = "Btn_Login";
            this.Btn_Login.Size = new System.Drawing.Size(91, 29);
            this.Btn_Login.TabIndex = 2;
            this.Btn_Login.Text = "התחבר";
            this.Btn_Login.UseVisualStyleBackColor = true;
            this.Btn_Login.Click += new System.EventHandler(this.Btn_Login_Click);
            // 
            // Lbl_User
            // 
            this.Lbl_User.AutoSize = true;
            this.Lbl_User.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_User.Location = new System.Drawing.Point(235, 126);
            this.Lbl_User.Name = "Lbl_User";
            this.Lbl_User.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_User.Size = new System.Drawing.Size(131, 23);
            this.Lbl_User.TabIndex = 4;
            this.Lbl_User.Text = "שם משתמש:";
            // 
            // Lbl_Pass
            // 
            this.Lbl_Pass.AutoSize = true;
            this.Lbl_Pass.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Lbl_Pass.Location = new System.Drawing.Point(282, 184);
            this.Lbl_Pass.Name = "Lbl_Pass";
            this.Lbl_Pass.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Lbl_Pass.Size = new System.Drawing.Size(84, 23);
            this.Lbl_Pass.TabIndex = 5;
            this.Lbl_Pass.Text = "סיסמא:";
            // 
            // Txt_User
            // 
            this.Txt_User.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_User.Location = new System.Drawing.Point(74, 126);
            this.Txt_User.Name = "Txt_User";
            this.Txt_User.Size = new System.Drawing.Size(124, 22);
            this.Txt_User.TabIndex = 0;
            // 
            // Txt_Pass
            // 
            this.Txt_Pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Pass.Location = new System.Drawing.Point(74, 185);
            this.Txt_Pass.Name = "Txt_Pass";
            this.Txt_Pass.PasswordChar = '*';
            this.Txt_Pass.Size = new System.Drawing.Size(124, 22);
            this.Txt_Pass.TabIndex = 1;
            // 
            // Btn_Canacel
            // 
            this.Btn_Canacel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Canacel.Location = new System.Drawing.Point(235, 242);
            this.Btn_Canacel.Name = "Btn_Canacel";
            this.Btn_Canacel.Size = new System.Drawing.Size(91, 29);
            this.Btn_Canacel.TabIndex = 3;
            this.Btn_Canacel.Text = "יציאה";
            this.Btn_Canacel.UseVisualStyleBackColor = true;
            this.Btn_Canacel.Click += new System.EventHandler(this.Btn_Canacel_Click);
            // 
            // ManageLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BuildingNorms.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(395, 307);
            this.Controls.Add(this.Btn_Canacel);
            this.Controls.Add(this.Txt_Pass);
            this.Controls.Add(this.Txt_User);
            this.Controls.Add(this.Lbl_Pass);
            this.Controls.Add(this.Lbl_User);
            this.Controls.Add(this.Btn_Login);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Location = new System.Drawing.Point(200, 200);
            this.Name = "ManageLogin";
            this.Text = "ManageLogin";
            this.Load += new System.EventHandler(this.ManageLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Login;
        private System.Windows.Forms.Label Lbl_User;
        private System.Windows.Forms.Label Lbl_Pass;
        private System.Windows.Forms.TextBox Txt_User;
        private System.Windows.Forms.TextBox Txt_Pass;
        private System.Windows.Forms.Button Btn_Canacel;
    }
}