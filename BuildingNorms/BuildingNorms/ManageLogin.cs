﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuildingNorms
{
    public partial class ManageLogin : Form
    {
        Thread th;
        public ManageLogin()
        {
            InitializeComponent();
        }

        private void Btn_Login_Click(object sender, EventArgs e)
        {
            if (Txt_User.Text != "" && Txt_Pass.Text != "")
            {
                if (VerifyLogin())
                {
                    Close();
                    th = new Thread(OpenManage);
                    th.SetApartmentState(ApartmentState.STA);
                    th.Start();
                }
                else
                {
                    MessageBox.Show("שם המשתמש או הסיסמא אינם נכונים. אנא נסה שנית. ");
                }
            }
            else
            {
                MessageBox.Show("שם המשתמש והסיסמא אינם יכולים להיות ריקים! אנא נסה שנית.  ");
            }
        }

        private bool VerifyLogin()
        {
            DbServiceSQL dbs = new DbServiceSQL();
            DataTable DT = new DataTable();
            string StrSql = "Select * from Drain_Permissions where user_name = '" + Txt_User.Text.Trim() + "' AND pass='" + Txt_Pass.Text.Trim() + "' ";
            DT = dbs.executeSelectQueryNoParam(StrSql);
            if (DT.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        private void OpenManage()
        {
            Application.Run(new Form1());
        }

        private void Btn_Canacel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ManageLogin_Load(object sender, EventArgs e)
        {
            Txt_User.Focus();
        }
    }
}
