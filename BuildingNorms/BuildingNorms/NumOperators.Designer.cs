﻿namespace BuildingNorms
{
    partial class NumOperators
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Btn_Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_NumOfOperators = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Txt_Mach = new System.Windows.Forms.TextBox();
            this.Btn_Save = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Btn_Close
            // 
            this.Btn_Close.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Close.Location = new System.Drawing.Point(6, 188);
            this.Btn_Close.Name = "Btn_Close";
            this.Btn_Close.Size = new System.Drawing.Size(129, 30);
            this.Btn_Close.TabIndex = 253;
            this.Btn_Close.Text = "צא";
            this.Btn_Close.UseVisualStyleBackColor = true;
            this.Btn_Close.Click += new System.EventHandler(this.Btn_Close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(71, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(156, 19);
            this.label1.TabIndex = 254;
            this.label1.Text = "עדכון כמות עובדים";
            // 
            // Txt_NumOfOperators
            // 
            this.Txt_NumOfOperators.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_NumOfOperators.Location = new System.Drawing.Point(6, 122);
            this.Txt_NumOfOperators.Name = "Txt_NumOfOperators";
            this.Txt_NumOfOperators.Size = new System.Drawing.Size(129, 30);
            this.Txt_NumOfOperators.TabIndex = 260;
            this.Txt_NumOfOperators.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Txt_NumOfOperators.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txt_NumOfOperators_KeyPress);
            this.Txt_NumOfOperators.Leave += new System.EventHandler(this.Txt_NumOfOperators_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label4.Location = new System.Drawing.Point(14, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 19);
            this.label4.TabIndex = 258;
            this.label4.Text = "כמות עובדים";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(196, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 19);
            this.label3.TabIndex = 257;
            this.label3.Text = "מכונה";
            // 
            // Txt_Mach
            // 
            this.Txt_Mach.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Txt_Mach.Location = new System.Drawing.Point(154, 122);
            this.Txt_Mach.Name = "Txt_Mach";
            this.Txt_Mach.ReadOnly = true;
            this.Txt_Mach.Size = new System.Drawing.Size(129, 30);
            this.Txt_Mach.TabIndex = 262;
            this.Txt_Mach.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Btn_Save
            // 
            this.Btn_Save.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.Btn_Save.Location = new System.Drawing.Point(154, 188);
            this.Btn_Save.Name = "Btn_Save";
            this.Btn_Save.Size = new System.Drawing.Size(129, 30);
            this.Btn_Save.TabIndex = 263;
            this.Btn_Save.Text = "שמור";
            this.Btn_Save.UseVisualStyleBackColor = true;
            this.Btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // NumOperators
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BuildingNorms.Properties.Resources.ATG_Wallpaper_1440x900;
            this.ClientSize = new System.Drawing.Size(293, 232);
            this.Controls.Add(this.Btn_Save);
            this.Controls.Add(this.Txt_Mach);
            this.Controls.Add(this.Txt_NumOfOperators);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Btn_Close);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "NumOperators";
            this.Text = "NumOperators";
            this.Load += new System.EventHandler(this.NumOperators_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_NumOfOperators;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Txt_Mach;
        private System.Windows.Forms.Button Btn_Save;
    }
}