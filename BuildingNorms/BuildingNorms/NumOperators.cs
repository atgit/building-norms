﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuildingNorms
{
    public partial class NumOperators : Form
    {
        string mach;
        double numOfOperators;
        public NumOperators()
        {
            InitializeComponent();
        }

        public string Mach { get => mach; set => mach = value; }
        public double NumOfOperators { get => numOfOperators; set => numOfOperators = value; }

        private void Btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void NumOperators_Load(object sender, EventArgs e)
        {
            Txt_Mach.Text = Mach;
            Txt_NumOfOperators.Text = NumOfOperators.ToString();
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            DBService dbs = new DBService();
            DialogResult D = MessageBox.Show(string.Format("?{0} אתה בטוח שאתה רוצה לעדכן את כמות העובדים למכונה ", Mach));
            if (D == DialogResult.OK) // נעדכן את כמות העובדים לכל הרשומות שכוללות את המכונה
            {
                // עדכון כמות המפעילים על כל המכונות הרלוונטיות
                string StrSql = $@"UPDATE BPCSDALI.FRTML01
                                  SET RMOPR ={NumOfOperators}
                                  WHERE RMMACI= '{ Mach }' AND RMWRKC BETWEEN 750000 AND 760000 ";
                dbs.executeUpdateQuery(StrSql);
                //UpdateMachineNeedNewNorm();
                MessageBox.Show("העדכון בוצע בהצלחה");
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        /// <summary>
        /// מעדכן את המכונה בקובץ המכונות כדי לסמן שהשתנו כמות עובדים ולכן צריך לעדכן את התקן
        /// </summary>
        public void UpdateMachineNeedNewNorm()
        {
            string StrSql = $@"UPDATE BPCSFALI.LMHOL01 
                              SET MOFRE3='1' 
                              WHERE MOMACH={Mach} AND MOWRKC BETWEEN 750000 AND 760000";
            DBService dbs = new DBService();
            dbs.executeUpdateQuery(StrSql);
        }

        private void Txt_NumOfOperators_Leave(object sender, EventArgs e)
        {
            NumOfOperators = double.Parse(Txt_NumOfOperators.Text);
        }

        private void Txt_NumOfOperators_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
       (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }
    }
}
